<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class ApisController extends AppController {

    public $uses = array('FeaturedProduct', 'User', 'ProductSpec', 'Product', 'ProductShipping',
        'Category', 'ProductCategory', 'OtherUser', 'ProductImage', 'StyleCuration',
        'UserStyleAction', 'UserFavorite', 'CartItem', 'Stylequiz', 'ProductWhat', 'UserDesignBoard',
        'ProductWhatSlider', 'Style', 'OtherUserShippingAdd', 'StyleOption', 'StyleImage', 'Order', 'OrderDetail', 'UserStyle');
    public $components = array(
        'Stripe.Stripe'
    );

    public function beforeFilter() {
        parent::beforeFilter();
        $this->autoRender = false;
        //$_SERVER['HTTP_AUTHORIZATION'] = $_SERVER['REDIRECT_REDIRECT_HTTP_AUTHORIZATION'];
    }

    function createStripCustomer() {
        Configure::write('debug', 2);
        $data = array(
            // 'stripeToken' => 'tok_0NAEASV7h0m7ny',
            'description' => 'Casi Robot',
            'email' => 'casi@robot.com'
        );

        $result = $this->Stripe->customerCreate($data);
    }

    function createCard() {
        $result = $this->Stripe->customerRetrieve('cus_8gShAPJWgsujMK');
        //  $Token = $this->Stripe->createToken('');
        // $result->sources->create(array("source" => $Token['stripe_id']));
        print_R($result);
    }

    function get() {
        App::import(
                'Vendor', 'ExpiredException', array('file' => 'firebase' . DS . 'php-jwt' . DS . 'src' . DS . 'ExpiredException.php')
        );
        App::import(
                'Vendor', 'BeforeValidException', array('file' => 'firebase' . DS . 'php-jwt' . DS . 'src' . DS . 'BeforeValidException.php')
        );
        App::import(
                'Vendor', 'SignatureInvalidException', array('file' => 'firebase' . DS . 'php-jwt' . DS . 'src' . DS . 'SignatureInvalidException.php')
        );
        App::import(
                'Vendor', 'JWT', array('file' => 'firebase' . DS . 'php-jwt' . DS . 'src' . DS . 'JWT.php')
        );

        $post_decode = $this->request->input('json_decode', true);
        $secretKey = Configure::read('Security.salt');
        if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
            try {
                $token2 = Firebase\JWT\JWT::decode(str_replace("Bearer ", "", $_SERVER['HTTP_AUTHORIZATION']), $secretKey, array('HS256'));
                print_R($token2);
            } catch (Exception $ex) {
                echo $ex->getMessage() . "\n\n\n\n";
            }

//print_R($token2);
        }





        $user = $this->OtherUser->find('first', array('conditions' => array('email' => $post_decode['email'], 'password' => md5($post_decode['password'])), 'recursive' => -1));

        $issuedAt = time();
        $notBefore = $issuedAt + 10;             //Adding 10 seconds
        $expire = $notBefore + 60;            // Adding 60 seconds

        $data = array(
            'iat' => $issuedAt, // Issued at: time when the token was generated           
            'nbf' => $notBefore, // Not before
            'exp' => $expire, // Expire
            'data' => array(// Data related to the signer user
                'userId' => $user['OtherUser']['id'], // userid from the users table
                'userName' => $user['OtherUser']['username'], // User name
            )
        );


        echo $token = Firebase\JWT\JWT::encode($data, $secretKey);
        exit;
    }

    public function signin() {
//mail("praveshphp@gmail.com","asda","asd");
//Configure::write('debug',2);

        $post_decode = $this->request->input('json_decode', true);

//devicetoken,device_model, login_type
        if ($post_decode['login_type'] == 'FB') {
            if ($post_decode['socialAccount_id'] != '') {
                $user = $this->OtherUser->find('first', array('conditions' => array('social_id' => $post_decode['socialAccount_id']), 'recursive' => -1));
                if ($user) {
                    $styles = $this->UserStyle->find('count', array('conditions' => array('UserStyle.other_user_id' => $user['OtherUser']['id'])));
                    $quiz_played = 0;
                    if ($styles > 0) {
                        $quiz_played = 1;
                    }
                    $user['OtherUser']['quiz_played'] = $quiz_played;
                    echo $this->json_encode(array('status' => 'success', 'data' => $user['OtherUser']));
                    $arr['OtherUser']['device_token'] = $post_decode['devicetoken'];
                    $arr['OtherUser']['device_model'] = $post_decode['device_model'];
                    $arr['OtherUser']['id'] = $user['OtherUser']['id'];
                    $id = $this->OtherUser->save($arr);
                } else {
                    echo $this->json_encode(array('status' => 'fail', 'message' => 'Please signup first.'));
                    exit;
                }
            } else {
                echo $this->json_encode(array('status' => 'fail', 'message' => 'Please signup first.'));
                exit;
            }
        } else {
            $user = $this->OtherUser->find('first', array('conditions' => array('email' => $post_decode['email'], 'password' => md5($post_decode['password'])), 'recursive' => -1));

            if ($user) {
                $arr['OtherUser']['device_token'] = $post_decode['devicetoken'];
                $arr['OtherUser']['device_model'] = $post_decode['device_model'];
                $arr['OtherUser']['id'] = $user['OtherUser']['id'];
                $id = $this->OtherUser->save($arr);
                $styles = $this->UserStyle->find('count', array('conditions' => array('UserStyle.other_user_id' => $user['OtherUser']['id'])));
                $quiz_played = 0;
                if ($styles > 0) {
                    $quiz_played = 1;
                }
                $user['OtherUser']['quiz_played'] = $quiz_played;
                echo $this->json_encode(array('status' => 'success', 'data' => $user['OtherUser']));
            } else {
                echo $this->json_encode(array('status' => 'fail', 'message' => 'User name/password incorrect'));
            }
            die();
        }
    }

    public function signup() {
        header('Content-type: application/json');
        //   $post_decode = $this->request->input('json_decode', true);
        $post_decode = $this->request->data;


        $arr['OtherUser']['firstName'] = $post_decode['firstname'];
        $arr['OtherUser']['lastName'] = $post_decode['lastname'];
        $arr['OtherUser']['email'] = $post_decode['email'];
        if ($post_decode['password'] != '') {
            $arr['OtherUser']['password'] = md5($post_decode['password']);
        }
        if ($post_decode['signup_type'] == 'FB') {
            $arr['OtherUser']['social_id'] = ($post_decode['socialAccount_id']);
            $arr['OtherUser']['username'] = ($post_decode['socialAccount_id']);
        } else {
            $arr['OtherUser']['username'] = $post_decode['email'];
        }
        if ($arr['OtherUser']['username'] == '') {
            echo $this->json_encode(array('status' => 'fail', 'message' => 'error'));
            exit;
        }
        $arr['OtherUser']['login_type'] = $post_decode['signup_type'];

        $arr['OtherUser']['phone'] = $post_decode['mobilenumber'];
        $arr['OtherUser']['device_token'] = $post_decode['devicetoken'];
        $arr['OtherUser']['device_model'] = $post_decode['device_model'];
        $arr['OtherUser']['profile_pic_url'] = @$post_decode['profile_pic_url'];
        $arr['OtherUser']['name'] = $arr['OtherUser']['firstName'] . " " . $arr['OtherUser']['lastName'];

        if ($post_decode['signup_type'] == 'FB') {
            $user = $this->OtherUser->find('first', array('conditions' => array('social_id' => $post_decode['socialAccount_id']), 'recursive' => -1));
            if ($user) {
                echo $this->json_encode(array('status' => 'success', 'data' => $user['OtherUser']));
            } else {
                $id = $this->OtherUser->save($arr);
                echo $this->json_encode(array('status' => 'success', 'data' => $id['OtherUser']));
            }
        } else {
            $user = $this->OtherUser->find('first', array('conditions' => array('email' => $post_decode['email'], 'password' => md5($post_decode['password'])), 'recursive' => -1));
//print_R($user);exit;

            if ($user) {
                echo $this->json_encode(array('status' => 'success', 'data' => $user['OtherUser']));
            } else {

                if (isset($this->request->params['form']['image']['name']) && $this->request->params['form']['image']['name'] != '') {

                    App::uses('S3', 'Vendor');
                    $bucket = "eyestylist";
                    if (!class_exists('S3'))
                        require_once('S3.php');
                    if (!defined('awsAccessKey'))
                        define('awsAccessKey', 'AKIAIZLIIN2ROT4M64CQ');
                    if (!defined('awsSecretKey'))
                        define('awsSecretKey', '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');
                    $s3 = new S3(awsAccessKey, awsSecretKey);
                    $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
                    $file = "Profile_" . time() . '_' . $this->request->params['form']['image']['name'];
                    $s3->putObjectFile($this->request->params['form']['image']['tmp_name'], $bucket, $file, S3::ACL_PUBLIC_READ);
                    $file_name = 'http://' . $bucket . '.s3.amazonaws.com/' . $file;
                    $arr['OtherUser']['profile_pic_url'] = $file_name;
                }


                $i = $this->OtherUser->save($arr);
                echo $this->json_encode(array('status' => 'success', 'data' => $i['OtherUser']));
                exit;
            }
        }
    }

    function getCategories() {
        /* {"cat_id":"1"}   cat_id Optional */
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['cat_id']) && $post_decode['cat_id'] != '') {
            $category_id = $post_decode['cat_id'];
        } else {
            $category_id = null;
        }


        $this->Category->unbindModel(array('hasAndBelongsToMany' => array('Product')));
        $this->Category->unbindModel(array('belongsTo' => array('ParentCategory')));
        if ($category_id == null) {
            $categories = $this->Category->find('all', array('conditions' => array('Category.level' => 0), 'recursive' => 1));
// print_r($categories);
            $arr = array();
            foreach ($categories as $k => $category) {
                $arr[$k]['id'] = $category['Category']['id'];
                $arr[$k]['parent_id'] = $category['Category']['parent_id'];
                $arr[$k]['name'] = $category['Category']['name'];
                $arr[$k]['cat_images'] = $category['Category']['cat_images'];
                $arr[$k]['hasChild'] = (!empty($category['ChildCategory'])) ? true : false;
            }
        } else {
            $categories = $this->Category->find('all', array('conditions' => array('Category.parent_id' => $category_id), 'recursive' => 1));
//  print_R($categories);
            $arr = array();
            foreach ($categories as $k => $category) {
                $arr[$k]['id'] = $category['Category']['id'];
                $arr[$k]['parent_id'] = $category['Category']['parent_id'];
                $arr[$k]['name'] = $category['Category']['name'];
                $arr[$k]['cat_images'] = $category['Category']['cat_images'];
                $arr[$k]['hasChild'] = (!empty($category['ChildCategory'])) ? true : false;
            }
        }
        echo $this->json_encode(array('status' => 'success', 'data' => $arr));
        exit;
    }

    function getProductsByCategory_old() {
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['cat_id']) && $post_decode['cat_id'] != '') {
            $category_id = $post_decode['cat_id'];
            $category_id = $this->Category->getAllSubSubCategories($post_decode['cat_id']);
        } else {
            $category_id = null;
        }
        if (isset($post_decode['other_user_id']) && $post_decode['other_user_id'] != '') {
            $other_user_id = $post_decode['other_user_id'];
        } else {
            $other_user_id = 0;
        }


        $joins = array();
        $joins[0] = array(
            'table' => 'product_categories',
            'alias' => 'ProductCategory',
            'type' => 'inner',
            'conditions' => array('ProductCategory.product_id= Product.id')
        );
        $joins[1] = array(
            'table' => 'categories',
            'alias' => 'Category',
            'type' => 'inner',
            'conditions' => array('Category.id =ProductCategory.category_id')
        );


        $this->Product->unbindModel(array('hasAndBelongsToMany' => array('Product')));
        $this->Product->unbindModel(array('hasAndBelongsToMany' => array('Category')));
        /* $this->Product->unbindModel(array('hasMany' => array('ProductImage')));  */


        /* $this->Product->bindModel(array('hasMany' => array('ProductImage' => array(
          'className' => 'ProductImage',
          'foreignKey' => 'product_id',
          'dependent' => true,
          'conditions' => 'ProductImage.addinroom=true',
          'fields' => '',
          'order' => '',
          'limit' => '',
          'offset' => '',
          'exclusive' => '',
          'finderQuery' => '',
          'counterQuery' => ''
          )))); */

        $this->Product->bindModel(array('hasOne' => array('UserFavorite' => array(
                    'className' => 'UserFavorite',
                    'foreignKey' => 'product_id',
                    'dependent' => true,
                    'conditions' => "UserFavorite.other_user_id={$other_user_id}",
                    'fields' => '',
                    'order' => '',
                    'limit' => '',
                    'offset' => '',
                    'exclusive' => '',
                    'finderQuery' => '',
                    'counterQuery' => ''
        ))));
        $product = $this->Product->find('all', array(
            'joins' => $joins,
            'conditions' => array('Category.id' => $category_id),
            'recursive' => 1,
            'fields' => array('Product.id', 'Product.prod_name', 'Product.price', 'Product.available_units', 'Category.id', 'UserFavorite.status'))
        );
//print_R($product);
        $arr = array();
        foreach ($product as $k => $p) {
            if ($product[$k]['Product']['price'] == null) {
                $product[$k]['Product']['price'] = '0.00';
            }
            if (isset($product[$k]['ProductImage'][0]['prod_img'])) {

                $sortArray = array();
                $people = $product[$k]['ProductImage'];
                foreach ($people as $person) {
                    foreach ($person as $key1 => $value) {
                        if (!isset($sortArray[$key1])) {
                            $sortArray[$key1] = array();
                        }
                        $sortArray[$key1][] = $value;
                    }
                }
                $orderby = "mainImage";
                array_multisort($sortArray[$orderby], SORT_DESC, $people);
                $product[$k]['ProductImage'] = $people;

                foreach ($product[$k]['ProductImage'] as $pi) {
                    if (strpos($pi['prod_img'], 'FRONT') !== false) {
                        $product[$k]['Product']['prod_img'] = $pi['prod_img'];
                        $product[$k]['Product']['prod_thumb'] = $pi['prod_thumb'];
                    }
                }
                if (!isset($product[$k]['Product']['prod_img'])) {
                    foreach ($product[$k]['ProductImage'] as $pi) {
                        if (strpos($pi['prod_img'], 'SIDE') !== false) {
                            $product[$k]['Product']['prod_img'] = $pi['prod_img'];
                            $product[$k]['Product']['prod_thumb'] = $pi['prod_thumb'];
                        }
                    }
                }
                if (!isset($product[$k]['Product']['prod_img'])) {
                    foreach ($product[$k]['ProductImage'] as $pi) {
                        if (strpos($pi['prod_img'], 'BACK') !== false) {
                            $product[$k]['Product']['prod_img'] = $pi['prod_img'];
                            $product[$k]['Product']['prod_thumb'] = $pi['prod_thumb'];
                        }
                    }
                }

                if (!isset($product[$k]['Product']['prod_img'])) {
                    $product[$k]['Product']['prod_img'] = $product[$k]['ProductImage'][0]['prod_img'];
                    $product[$k]['Product']['prod_thumb'] = $product[$k]['ProductImage'][0]['prod_thumb'];
                }
                if ($product[$k]['UserFavorite']['status'] == null) {
                    $product[$k]['Product']['favorite'] = 0;
                } else {
                    $product[$k]['Product']['favorite'] = 1;
                }
                unset($product[$k]['ProductImage']);
                $arr[] = $product[$k]['Product'];
            }
        }
        sort($arr);
        if (!empty($arr)) {
            echo $this->json_encode(array('status' => 'success', 'data' => $arr));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'data' => 'No products found.'));
        }
    }

    function getProductsByCategory() {
// Configure::write('debug',2);
//{"cat_id":"1","offset":0 ,"style_id":1,"filters":{"Price":0,"Styles":1,"Manner of shipping":"FEDEX/UPS","Metal Type":"ZINC ALLOY","Color":"BROWN","Dimensions":"17\" x 17\" x 23\""}}

        $post_decode = $this->request->input('json_decode', true);
//   print_R($post_decode['filters']['Price']);
        if (isset($post_decode['cat_id']) && $post_decode['cat_id'] != '') {
            $category_id = $post_decode['cat_id'];
            $category_id = $this->Category->getAllSubSubCategories($post_decode['cat_id']);
        } else {
            $category_id = null;
        }
        if (isset($post_decode['other_user_id']) && $post_decode['other_user_id'] != '') {
            $other_user_id = $post_decode['other_user_id'];
            $styles = $this->UserStyle->find('all', array('conditions' => array('UserStyle.other_user_id' => $other_user_id), 'order' => 'UserStyle.primary_style desc'));

            if ($styles) {
                $order_style = "";
                foreach ($styles as $style) {
                    $order_style .= " style{$style['UserStyle']['style_id']}_order desc, ";
                }
            } else {
                $order_style = "";
            }
        } else {
            $other_user_id = 0;
            $order_style = "";
        }
        $where = "";
        $order = "";
        if (isset($post_decode['filters']) && $post_decode['filters'] != '') {
            if (isset($post_decode['filters']['Styles']) && $post_decode['filters']['Styles'] != '') {
                $where .= " AND style{$post_decode['filters']['Styles']}='1'";
            }
            if (isset($post_decode['filters']['Manner of shipping']) && $post_decode['filters']['Manner of shipping'] != '') {
                $post_decode['filters']['Manner of shipping'] = addslashes($post_decode['filters']['Manner of shipping']);
                $where .= " AND ProductShipping.manner_of_shipping='{$post_decode['filters']['Manner of shipping']}'";
            }
            if (isset($post_decode['filters']['Metal Type']) && $post_decode['filters']['Metal Type'] != '') {
                $post_decode['filters']['Metal Type'] = addslashes($post_decode['filters']['Metal Type']);
                $where .= " AND Product.metal_type='{$post_decode['filters']['Metal Type']}'";
            }
            if (isset($post_decode['filters']['Fill Material']) && $post_decode['filters']['Fill Material'] != '') {
                $post_decode['filters']['Fill Material'] = addslashes($post_decode['filters']['Fill Material']);
                $where .= " AND Product.fill_material='{$post_decode['filters']['Fill Material']}'";
            }
            if (isset($post_decode['filters']['Frame Body Material']) && $post_decode['filters']['Frame Body Material'] != '') {
                $post_decode['filters']['Frame Body Material'] = addslashes($post_decode['filters']['Frame Body Material']);
                $where .= " AND Product.frame_body_material='{$post_decode['filters']['Frame Body Material']}'";
            }
            if (isset($post_decode['filters']['Upholestry Material']) && $post_decode['filters']['Upholestry Material'] != '') {
                $post_decode['filters']['Upholestry Material'] = addslashes($post_decode['filters']['Upholestry Material']);
                $where .= " AND Product.upholstery_material='{$post_decode['filters']['Upholestry Material']}'";
            }
            if (isset($post_decode['filters']['Rug Construction']) && $post_decode['filters']['Rug Construction'] != '') {
                $post_decode['filters']['Rug Construction'] = addslashes($post_decode['filters']['Rug Construction']);
                $where .= " AND Product.RugConstruction='{$post_decode['filters']['Rug Construction']}'";
            }
            if (isset($post_decode['filters']['Rug Type']) && $post_decode['filters']['Rug Type'] != '') {
                $post_decode['filters']['Rug Type'] = addslashes($post_decode['filters']['Rug Type']);
                $where .= " AND Product.RugType='{$post_decode['filters']['Rug Type']}'";
            }
            if (isset($post_decode['filters']['Color']) && $post_decode['filters']['Color'] != '') {
                $post_decode['filters']['Color'] = addslashes($post_decode['filters']['Color']);
                $where .= " AND ProductSpec.color='{$post_decode['filters']['Color']}'";
            }
            if (isset($post_decode['filters']['Dimensions']) && $post_decode['filters']['Dimensions'] != '') {
                $post_decode['filters']['Dimensions'] = addslashes($post_decode['filters']['Dimensions']);
                $where .= " AND ProductSpec.dimensions='{$post_decode['filters']['Dimensions']}'";
            }

            if (isset($post_decode['filters']['Price']) && $post_decode['filters']['Price'] !== '') {
                if ($post_decode['filters']['Price'] == 0) {
                    $order = "order by price asc ,  $order_style category_order desc";
                } else {
                    $order = "order by price desc ,  $order_style category_order desc";
                }
            } else {
                $order = "order by  $order_style category_order desc";
            }
        } else {
            if (isset($post_decode['order']) && $post_decode['order'] != '') {
                $order = "order by " . $post_decode['order'] . " , {$order_style} category_order desc";
            } else {
                $order = "order by  $order_style category_order desc";
            }

            if (isset($post_decode['style_id']) && $post_decode['style_id'] != '') {
                $where = " AND style{$post_decode['style_id']}='1'";
            }
        }


        $category_final = implode(",", $category_id);

        if (isset($post_decode['page']) && $post_decode['page'] != '') {
            $page = $post_decode['page'];
        } else {
            $page = 1;
        }
        $limit = 100;
        if (isset($post_decode['offset']) && $post_decode['offset'] != '') {
            $offset = $post_decode['offset'];
        } else {
            $offset = 0;
        }

        $product = $this->Product->query(
                "SELECT `id`,null as more_products,`prod_name`,parent_style_number,`price`,`available_units`,`status`, prod_img,prod_thumb,category_order,style2_order,style3_order,style4_order,style5_order FROM ("
                . "SELECT `Product`.`id`, `Product`.`prod_name`, `Product`.`price`,Product.parent_style_number , `Product`.`available_units`,  `UserFavorite`.`status`,"
                . "ProductImage.prod_img,ProductImage.prod_thumb,category_order,style1_order,style2_order,style3_order,style4_order,style5_order FROM `eyestylist`.`products` AS `Product` "
                . "LEFT JOIN `eyestylist`.`product_shippings` AS `ProductShipping` ON (`ProductShipping`.`product_id` = `Product`.`id`) "
                . "LEFT JOIN `eyestylist`.`product_specs` AS `ProductSpec` ON (`ProductSpec`.`product_id` = `Product`.`id`) "
                . "LEFT JOIN `eyestylist`.`user_favorites` AS `UserFavorite` ON (`UserFavorite`.`other_user_id`=0 AND `UserFavorite`.`product_id` = `Product`.`id`) "
                . "INNER JOIN `eyestylist`.`product_categories` AS `ProductCategory` ON (`ProductCategory`.`product_id`= `Product`.`id`) "
                . "INNER JOIN `eyestylist`.`categories` AS `Category` ON (`Category`.`id` =`ProductCategory`.`category_id`) "
                . "inner JOIN (SELECT * FROM `product_images` WHERE (`mainImage`=1 OR addinroom=1) ORDER BY `mainImage` DESC ,`addinroom` DESC) AS ProductImage ON ProductImage.product_id=Product.id  "
                . " WHERE `Category`.`id` IN ({$category_final}) $where GROUP BY ProductImage.`product_id` $order ) AS Product   "
                . "  $order LIMIT {$limit} OFFSET {$offset} ");

        $arr = array();
        foreach ($product as $k => $p) {
            if ($product[$k]['Product']['price'] == null) {
                $product[$k]['Product']['price'] = '0.00';
            }
            $arr[$k]['id'] = $product[$k]['Product']['id'];
            $arr[$k]['prod_name'] = $product[$k]['Product']['prod_name'];
            $arr[$k]['price'] = $product[$k]['Product']['price'];
            $arr[$k]['available_units'] = $product[$k]['Product']['available_units'];
            $arr[$k]['more_products'] = $product[$k][0]['more_products'];
            if (isset($product[$k]['Product']['prod_img'])) {
                $arr[$k]['prod_img'] = $product[$k]['Product']['prod_img'];
                $arr[$k]['prod_thumb'] = $product[$k]['Product']['prod_thumb'];

                if ($product[$k]['Product']['status'] == null) {
                    $arr[$k]['favorite'] = 0;
                } else {
                    $arr[$k]['favorite'] = 1;
                }
                unset($product[$k]['ProductImage']);
                unset($product[$k]['Product']);
            }
        }
        //$arr = array();
//        foreach ($product as $k => $p) {
//            if ($product[$k]['Product']['price'] == null) {
//                $product[$k]['Product']['price'] = '0.00';
//            }
//            if (isset($product[$k]['ProductImage']['prod_img'])) {
//
//
//
//                if (!isset($product[$k]['Product']['prod_img'])) {
//                    $product[$k]['Product']['prod_img'] = $product[$k]['ProductImage']['prod_img'];
//                    $product[$k]['Product']['prod_thumb'] = $product[$k]['ProductImage']['prod_thumb'];
//                }
//                if ($product[$k]['UserFavorite']['status'] == null) {
//                    $product[$k]['Product']['favorite'] = 0;
//                } else {
//                    $product[$k]['Product']['favorite'] = 1;
//                }
//                unset($product[$k]['ProductImage']);
//                $arr[] = $product[$k]['Product'];
//            }
//        }
//sort($arr);
        if (!empty($arr)) {
            echo $this->json_encode(array('status' => 'success', 'data' => $arr, 'offset' => $offset + $limit));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'data' => 'No products found.'));
        }
    }

    function cartToCamera() {
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['other_user_id'])) {
            $arr['CartItem']['other_user_id'] = $post_decode['other_user_id'];

            $allCart = $this->CartItem->find('all', array('recursive' => 0, 'conditions' => array('CartItem.other_user_id' => $post_decode['other_user_id']),
                'fields' => array('CartItem.id', 'CartItem.created', 'Product.id', 'Product.prod_name', 'CartItem.quantity')));
            $pro = array();
            foreach ($allCart as $c) {
                $pro[] = $c['Product']['id'];
            }
            if (isset($post_decode['page']) && $post_decode['page'] != '') {
                $page = $post_decode['page'];
            } else {
                $page = 1;
            }
            $limit = 100;
            if (isset($post_decode['offset']) && $post_decode['offset'] != '') {
                $offset = $post_decode['offset'];
            } else {
                $offset = 0;
            }
            if (!empty($pro)) {

                $proid = implode(",", array_filter($pro));
                // print_R($proid);exit;
                $sql = "SELECT `Product`.`id`,
                    `Product`.`prod_name`,
                    `Category`.`name`,
                    `Category`.`id`,
                    `ProductSpec`.`dimensions`,
                   `ProductImage`.`id`, 
                   `ProductImage`.`product_id`, 
                   `ProductImage`.`prod_img`, 
                   `ProductImage`.`prod_thumb`, 
                   `ProductImage`.`upc_number`, 
                   `ProductImage`.`addinroom`, 
                   `ProductImage`.`mainImage`, 
                   `ProductImage`.`image_type`
                    FROM `eyestylist`.`products` AS `Product`" .
// "LEFT JOIN `eyestylist`.`product_shippings` AS `ProductShipping` ON (`ProductShipping`.`product_id` = `Product`.`id`)". 
                        " LEFT JOIN `eyestylist`.`product_specs` AS `ProductSpec` ON (`ProductSpec`.`product_id` = `Product`.`id`) 
                   inner JOIN `eyestylist`.`product_categories` AS `ProductCategory` ON (`ProductCategory`.`product_id`= `Product`.`id`) 
                   inner JOIN `eyestylist`.`categories` AS `Category` ON (`Category`.`id` =`ProductCategory`.`category_id`)  
                   inner join `eyestylist`.`product_images` AS `ProductImage` on `ProductImage`.`product_id`=`Product`.`id` and `ProductImage`.`addinroom` IN (1)
                   WHERE `Product`.`id` IN ({$proid})   order by `ProductImage`.`mainImage` desc, `ProductImage`.`addinroom` desc LIMIT {$limit} OFFSET {$offset}";
                $product = $this->Product->query($sql);
                $arr = array();
                $j = 0;
                foreach ($product as $k => $p) {
//print_R($product[$k]);exit;

                    $arr['ProductImage'][$p['Product']['id']][$j] = $product[$k]['ProductImage'];
                    $arr['ProductImage'][$p['Product']['id']][$j]['prod_name'] = $product[$k]['Product']['prod_name'];
                    $arr['ProductImage'][$p['Product']['id']][$j]['cat_name'] = $product[$k]['Category']['name'];
                    $arr['ProductImage'][$p['Product']['id']][$j]['cat_id'] = $product[$k]['Category']['id'];
                    $c = $this->Category->getParents($product[$k]['Category']['id']);
                    $arr['ProductImage'][$p['Product']['id']][$j]['parent_cat_id'] = $c['Category'];
                    $arr['ProductImage'][$p['Product']['id']][$j]['dimensions'] = $product[$k]['ProductSpec']['dimensions'];
                    $j++;

                    unset($product[$k]['Product']);
                    unset($product[$k]['Category']);
                    unset($product[$k]['ProductSpec']);
                }
                $temp = array();
                if (!empty($arr['ProductImage'])) {
                    foreach ($arr['ProductImage'] as $newarray) {
                        $temp[]['ProductImage'] = array_values($newarray);
                    }
                }
// print_R($temp);
// exit;
                if (!empty($temp)) {
                    echo $this->json_encode(array('status' => 'success', 'data' => $temp, 'offset' => $offset + 100));
                } else {
                    echo $this->json_encode(array('status' => 'fail', 'data' => 'No products found.'));
                }
            }
        }
    }

    function getProductsByCategoryCamera() {
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['cat_id']) && $post_decode['cat_id'] != '') {
            $category_id = $post_decode['cat_id'];
            $category_id = $this->Category->getAllSubSubCategories($post_decode['cat_id']);
        } else {
            $category_id = null;
        }
        if (isset($post_decode['page']) && $post_decode['page'] != '') {
            $page = $post_decode['page'];
        } else {
            $page = 1;
        }
        $limit = 100;
        if (isset($post_decode['offset']) && $post_decode['offset'] != '') {
            $offset = $post_decode['offset'];
        } else {
            $offset = 0;
        }
        $category_id = implode(",", $category_id);
        /* $joins = array();
          $joins[0] = array(
          'table' => 'product_categories',
          'alias' => 'ProductCategory',
          'type' => 'inner',
          'conditions' => array('ProductCategory.product_id= Product.id')
          );
          $joins[1] = array(
          'table' => 'categories',
          'alias' => 'Category',
          'type' => 'inner',
          'conditions' => array('Category.id =ProductCategory.category_id',)
          );



          $this->Product->unbindModel(array('hasAndBelongsToMany' => array('Category')));
          // $this->Product->unbindModel(array('hasMany' => array('ProductImage')));

          $this->Product->bindModel(array('hasMany' => array('ProductImage' => array(
          'className' => 'ProductImage',
          'foreignKey' => 'product_id',
          'dependent' => true,
          'conditions' => 'ProductImage.addinroom=1',
          'fields' => '',
          'order' => '',
          'limit' => '',
          'offset' => '',
          'exclusive' => '',
          'finderQuery' => '',
          'counterQuery' => ''
          ))));

          $this->Product->hasMany['ProductImage']['conditions'] = array('ProductImage.addinroom' => array(0, 1));

          $product = $this->Product->find('all', array(
          'joins' => $joins,
          'conditions' => array('Category.id' => $category_id),
          'recursive' => 1,
          'fields' => array('Product.id', 'Product.prod_name', 'Category.name', 'Category.id', 'ProductSpec.dimensions'),
          'limit' => $limit,
          'offset' => $offset
          )
          );
          // $log = $this->Product->getDataSource()->getLog(false, false);
          //   print_R($log);
          //  exit;

          foreach ($product as $k => $p) {
          if (empty($product[$k]['ProductImage'])) {
          unset($product[$k]);
          }
          if (!empty($product[$k]['ProductImage'])) {
          foreach ($product[$k]['ProductImage'] as $kp => $pi) {
          $product[$k]['ProductImage'][$kp]['prod_name'] = $product[$k]['Product']['prod_name'];
          $product[$k]['ProductImage'][$kp]['cat_name'] = $product[$k]['Category']['name'];
          $product[$k]['ProductImage'][$kp]['cat_id'] = $product[$k]['Category']['id'];
          $c = $this->Category->getParents($product[$k]['Category']['id']);
          $product[$k]['ProductImage'][$kp]['parent_cat_id'] = $c['Category'];
          $product[$k]['ProductImage'][$kp]['dimensions'] = $product[$k]['ProductSpec']['dimensions'];

          // print_R( $c['Category']);exit;
          } //print_R($product[$k]['ProductImage']);
          //exit;
          $sortArray = array();
          $people = $product[$k]['ProductImage'];
          foreach ($people as $person) {
          foreach ($person as $key => $value) {
          if (!isset($sortArray[$key])) {
          $sortArray[$key] = array();
          }
          $sortArray[$key][] = $value;
          }
          }
          $orderby = "mainImage";
          array_multisort($sortArray[$orderby], SORT_DESC, $people);
          $product[$k]['ProductImage'] = $people;
          }
          unset($product[$k]['Product']);
          unset($product[$k]['Category']);
          unset($product[$k]['ProductSpec']);
          }
          sort($product); */
        $sql = "SELECT `Product`.`id`,
                    `Product`.`prod_name`,
                    `Category`.`name`,
                    `Category`.`id`,
                    `ProductSpec`.`dimensions`,
                   `ProductImage`.`id`, 
                   `ProductImage`.`product_id`, 
                   `ProductImage`.`prod_img`, 
                   `ProductImage`.`prod_thumb`, 
                   `ProductImage`.`upc_number`, 
                   `ProductImage`.`addinroom`, 
                   `ProductImage`.`mainImage`, 
                   `ProductImage`.`image_type`
                    FROM `eyestylist`.`products` AS `Product`" .
// "LEFT JOIN `eyestylist`.`product_shippings` AS `ProductShipping` ON (`ProductShipping`.`product_id` = `Product`.`id`)". 
                " LEFT JOIN `eyestylist`.`product_specs` AS `ProductSpec` ON (`ProductSpec`.`product_id` = `Product`.`id`) 
                   inner JOIN `eyestylist`.`product_categories` AS `ProductCategory` ON (`ProductCategory`.`product_id`= `Product`.`id`) 
                   inner JOIN `eyestylist`.`categories` AS `Category` ON (`Category`.`id` =`ProductCategory`.`category_id`)  
                   inner join `eyestylist`.`product_images` AS `ProductImage` on `ProductImage`.`product_id`=`Product`.`id` and `ProductImage`.`addinroom` IN (1)
                   WHERE `Category`.`id` IN ({$category_id})   order by `ProductImage`.`mainImage` desc, `ProductImage`.`addinroom` desc LIMIT {$limit} OFFSET {$offset}";
        $product = $this->Product->query($sql);
        $arr = array();
        $j = 0;
        foreach ($product as $k => $p) {
//print_R($product[$k]);exit;

            $arr['ProductImage'][$p['Product']['id']][$j] = $product[$k]['ProductImage'];
            $arr['ProductImage'][$p['Product']['id']][$j]['prod_name'] = $product[$k]['Product']['prod_name'];
            $arr['ProductImage'][$p['Product']['id']][$j]['cat_name'] = $product[$k]['Category']['name'];
            $arr['ProductImage'][$p['Product']['id']][$j]['cat_id'] = $product[$k]['Category']['id'];
            $c = $this->Category->getParents($product[$k]['Category']['id']);
            $arr['ProductImage'][$p['Product']['id']][$j]['parent_cat_id'] = $c['Category'];
            $arr['ProductImage'][$p['Product']['id']][$j]['dimensions'] = $product[$k]['ProductSpec']['dimensions'];
            $j++;

            unset($product[$k]['Product']);
            unset($product[$k]['Category']);
            unset($product[$k]['ProductSpec']);
        }
        $temp = array();
        if (!empty($arr['ProductImage'])) {
            foreach ($arr['ProductImage'] as $newarray) {
                $temp[]['ProductImage'] = array_values($newarray);
            }
        }
// print_R($temp);
// exit;
        if (!empty($temp)) {
            echo $this->json_encode(array('status' => 'success', 'data' => $temp, 'offset' => $offset + 100));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'data' => 'No products found.'));
        }
    }

    function getCartItemsInCamera() {
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['other_user_id']) && $post_decode['other_user_id'] != '') {

            // $this->CartItem->virtualFields = array('subtotal' => 'CartItem.quantity * Product.price');
            $allCart = $this->CartItem->find('all', array('recursive' => 0, 'conditions' => array('CartItem.other_user_id' => $post_decode['other_user_id']),
                'fields' => array('Product.id')));
            $parr = array();

            foreach ($allCart as $pro) {
                $parr[] = $pro['Product']['id'];
            }
            if (!empty($parr)) {
                $product_id = implode(",", $parr);
            } else {
                $product_id = '0';
            }
        } else {
            $product_id = '0';
        }
        if (isset($post_decode['page']) && $post_decode['page'] != '') {
            $page = $post_decode['page'];
        } else {
            $page = 1;
        }
        $limit = 100;
        if (isset($post_decode['offset']) && $post_decode['offset'] != '') {
            $offset = $post_decode['offset'];
        } else {
            $offset = 0;
        }


        $sql = "SELECT `Product`.`id`,
                    `Product`.`prod_name`,
                    `Category`.`name`,
                    `Category`.`id`,
                    `ProductSpec`.`dimensions`,
                   `ProductImage`.`id`, 
                   `ProductImage`.`product_id`, 
                   `ProductImage`.`prod_img`, 
                   `ProductImage`.`prod_thumb`, 
                   `ProductImage`.`upc_number`, 
                   `ProductImage`.`addinroom`, 
                   `ProductImage`.`mainImage`, 
                   `ProductImage`.`image_type`
                    FROM `eyestylist`.`products` AS `Product`" .
// "LEFT JOIN `eyestylist`.`product_shippings` AS `ProductShipping` ON (`ProductShipping`.`product_id` = `Product`.`id`)". 
                " LEFT JOIN `eyestylist`.`product_specs` AS `ProductSpec` ON (`ProductSpec`.`product_id` = `Product`.`id`) 
                   inner JOIN `eyestylist`.`product_categories` AS `ProductCategory` ON (`ProductCategory`.`product_id`= `Product`.`id`) 
                   inner JOIN `eyestylist`.`categories` AS `Category` ON (`Category`.`id` =`ProductCategory`.`category_id`)  
                   inner join `eyestylist`.`product_images` AS `ProductImage` on `ProductImage`.`product_id`=`Product`.`id` and `ProductImage`.`addinroom` IN (1)
                   WHERE `Product`.`id` IN ({$product_id})   order by `ProductImage`.`mainImage` desc, `ProductImage`.`addinroom` desc LIMIT {$limit} OFFSET {$offset}";
        $product = $this->Product->query($sql);
        $arr = array();
        $j = 0;
        foreach ($product as $k => $p) {
//print_R($product[$k]);exit;

            $arr['ProductImage'][$p['Product']['id']][$j] = $product[$k]['ProductImage'];
            $arr['ProductImage'][$p['Product']['id']][$j]['prod_name'] = $product[$k]['Product']['prod_name'];
            $arr['ProductImage'][$p['Product']['id']][$j]['cat_name'] = $product[$k]['Category']['name'];
            $arr['ProductImage'][$p['Product']['id']][$j]['cat_id'] = $product[$k]['Category']['id'];
            $c = $this->Category->getParents($product[$k]['Category']['id']);
            $arr['ProductImage'][$p['Product']['id']][$j]['parent_cat_id'] = $c['Category'];
            $arr['ProductImage'][$p['Product']['id']][$j]['dimensions'] = $product[$k]['ProductSpec']['dimensions'];
            $j++;

            unset($product[$k]['Product']);
            unset($product[$k]['Category']);
            unset($product[$k]['ProductSpec']);
        }
        $temp = array();
        if (!empty($arr['ProductImage'])) {
            foreach ($arr['ProductImage'] as $newarray) {
                $temp[]['ProductImage'] = array_values($newarray);
            }
        }
// print_R($temp);
// exit;
        if (!empty($temp)) {
            echo $this->json_encode(array('status' => 'success', 'data' => $temp, 'offset' => $offset + 100));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'data' => 'No products found.'));
        }
    }

    function getProductsByIDCamera() {
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['product_id']) && $post_decode['product_id'] != '') {
            $product_id = $post_decode['product_id'];
        } else {
            $product_id = null;
        }
        if (isset($post_decode['page']) && $post_decode['page'] != '') {
            $page = $post_decode['page'];
        } else {
            $page = 1;
        }
        $limit = 100;
        if (isset($post_decode['offset']) && $post_decode['offset'] != '') {
            $offset = $post_decode['offset'];
        } else {
            $offset = 0;
        }

        /* $joins = array();
          $joins[0] = array(
          'table' => 'product_categories',
          'alias' => 'ProductCategory',
          'type' => 'inner',
          'conditions' => array('ProductCategory.product_id= Product.id')
          );
          $joins[1] = array(
          'table' => 'categories',
          'alias' => 'Category',
          'type' => 'inner',
          'conditions' => array('Category.id =ProductCategory.category_id',)
          );

          // $this->Product->unbindModel(array('hasAndBelongsToMany' => array('Product')));
          $this->Product->unbindModel(array('hasAndBelongsToMany' => array('Category')));
          // $this->Product->unbindModel(array('hasMany' => array('ProductImage')));

          $this->Product->bindModel(array('hasMany' => array('ProductImage' => array(
          'className' => 'ProductImage',
          'foreignKey' => 'product_id',
          'dependent' => true,
          'conditions' => 'ProductImage.addinroom=1',
          'fields' => '',
          'order' => '',
          'limit' => '',
          'offset' => '',
          'exclusive' => '',
          'finderQuery' => '',
          'counterQuery' => ''
          ))));


          $product = $this->Product->find('all', array(
          'joins' => $joins,
          'conditions' => array('Product.id' => $product_id),
          'recursive' => 1,
          'fields' => array('Product.id', 'Product.prod_name', 'Category.name', 'Category.id', 'ProductSpec.dimensions'),
          'limit' => $limit,
          'offset' => $offset
          )
          );


          //$log = $this->Product->getDataSource()->getLog(false, false);
          // debug($log);
          // exit;
          // print_R($product);
          foreach ($product as $k => $p) {
          if (empty($product[$k]['ProductImage'])) {
          unset($product[$k]);
          }
          if (!empty($product[$k]['ProductImage'])) {
          foreach ($product[$k]['ProductImage'] as $kp => $pi) {
          $product[$k]['ProductImage'][$kp]['prod_name'] = $product[$k]['Product']['prod_name'];
          $product[$k]['ProductImage'][$kp]['cat_name'] = $product[$k]['Category']['name'];
          $product[$k]['ProductImage'][$kp]['cat_id'] = $product[$k]['Category']['id'];
          $c = $this->Category->getParents($product[$k]['Category']['id']);
          $product[$k]['ProductImage'][$kp]['parent_cat_id'] = $c['Category'];
          $product[$k]['ProductImage'][$kp]['dimensions'] = $product[$k]['ProductSpec']['dimensions'];

          // print_R( $c['Category']);exit;
          } //print_R($product[$k]['ProductImage']);
          //exit;
          $sortArray = array();
          $people = $product[$k]['ProductImage'];
          foreach ($people as $person) {
          foreach ($person as $key => $value) {
          if (!isset($sortArray[$key])) {
          $sortArray[$key] = array();
          }
          $sortArray[$key][] = $value;
          }
          }
          $orderby = "mainImage";
          array_multisort($sortArray[$orderby], SORT_DESC, $people);
          $product[$k]['ProductImage'] = $people;
          }
          unset($product[$k]['Product']);
          unset($product[$k]['Category']);
          unset($product[$k]['ProductSpec']);
          }
          sort($product);
          if (!empty($product)) {
          echo $this->json_encode(array('status' => 'success', 'data' => $product, 'offset' => $offset + 100));
          } else {
          echo $this->json_encode(array('status' => 'fail', 'data' => 'No products found.'));
          }
         */

        $sql = "SELECT `Product`.`id`, 
                `Product`.`prod_name`, 
                `Category`.`name`, 
                `Category`.`id`, 
                `ProductSpec`.`dimensions` ,
                `ProductImage`.`id`, 
                `ProductImage`.`product_id`, 
                `ProductImage`.`prod_img`, 
                `ProductImage`.`prod_thumb`, 
                `ProductImage`.`upc_number`, 
                `ProductImage`.`addinroom`, 
                `ProductImage`.`mainImage`, 
                `ProductImage`.`image_type`
                FROM `eyestylist`.`products` AS `Product` 

                LEFT JOIN `eyestylist`.`product_specs` AS `ProductSpec` ON (`ProductSpec`.`product_id` = `Product`.`id`) 
                inner JOIN `eyestylist`.`product_categories` AS `ProductCategory` ON (`ProductCategory`.`product_id`= `Product`.`id`) 
                inner JOIN `eyestylist`.`categories` AS `Category` ON (`Category`.`id` =`ProductCategory`.`category_id`)  
                inner join `eyestylist`.`product_images` AS `ProductImage` on `ProductImage`.`product_id` =`Product`.`id` and (`ProductImage`.`addinroom`=1 OR
                `ProductImage`.`mainImage`=1)
                WHERE `Product`.`id` = {$product_id} order by `ProductImage`.`mainImage` desc   LIMIT {$limit} OFFSET {$offset}
           ";
        $product = $this->Product->query($sql);

        $arr = array();
        foreach ($product as $k => $p) {

//exit;
            $product[$k]['ProductImage']['prod_name'] = $product[$k]['Product']['prod_name'];
            $product[$k]['ProductImage']['cat_name'] = $product[$k]['Category']['name'];
            $product[$k]['ProductImage']['cat_id'] = $product[$k]['Category']['id'];
            $c = $this->Category->getParents($product[$k]['Category']['id']);
            $product[$k]['ProductImage']['parent_cat_id'] = $c['Category'];
            $product[$k]['ProductImage']['dimensions'] = $product[$k]['ProductSpec']['dimensions'];
            unset($product[$k]['Product']);
            unset($product[$k]['Category']);
            unset($product[$k]['ProductSpec']);
            $arr['ProductImage'][$k] = $product[$k]['ProductImage'];
            unset($product[$k]['ProductImage']);
        }



        if (!empty($arr)) {
            echo $this->json_encode(array('status' => 'success', 'data' => $arr, 'offset' => $offset + 100));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'data' => 'No products found.'));
        }
    }

    public function productSearchKeyword() {
        $post_decode = $this->request->input('json_decode', true);
        $keyword = $post_decode['keyword'];
        if (strlen($keyword) > 2) {

            $product = $this->Product->find('all', array('conditions' => array('Product.prod_name like' => "%{$keyword}%"), 'recursive' => 1, 'fields' => array('Product.prod_name', 'Product.id')));
            $category = $this->Category->find('all', array('conditions' => array('Category.name like' => "%{$keyword}%"), 'recursive' => -1, 'fields' => array('Category.name', 'Category.id', 'Category.cat_images')));
            $arr = array();
            $i = 0;

            foreach ($product as $k => $p) {
                if (isset($product[$k]['ProductImage'][0]['prod_img']) && $product[$k]['ProductImage'][0]['prod_img'] != '') {
                    $sortArray = array();
                    $people = $product[$k]['ProductImage'];
                    foreach ($people as $person) {
                        foreach ($person as $key1 => $value) {
                            if (!isset($sortArray[$key1])) {
                                $sortArray[$key1] = array();
                            }
                            $sortArray[$key1][] = $value;
                        }
                    }
                    $orderby = "mainImage";
                    array_multisort($sortArray[$orderby], SORT_DESC, $people);
                    $product[$k]['ProductImage'] = $people;

                    $arr[$i]['name'] = $p['Product']['prod_name'];
                    $arr[$i]['id'] = $p['Product']['id'];
                    $arr[$i]['category'] = 0;
                    $arr[$i]['image'] = $product[$k]['ProductImage'][0]['prod_img'];
                    $i++;
                }
            }
            foreach ($category as $c) {
                $arr[$i]['name'] = $c['Category']['name'];
                $arr[$i]['id'] = $c['Category']['id'];
                $arr[$i]['category'] = 1;
                $arr[$i]['image'] = $c['Category']['cat_images'];
                $i++;
            }
            sort($arr);

            if (!empty($arr)) {
                echo $this->json_encode(array('status' => 'success', 'data' => $arr));
            } else {
                echo $this->json_encode(array('status' => 'fail', 'data' => 'No match found!'));
            }
        } else {
            echo $this->json_encode(array('status' => 'fail', 'data' => 'Please enter more than 2 characters'));
        }
    }

    public function productSearch() {
        /* {"keyword":"asd","pageno":"1"}   pageno Optional */
        $post_decode = $this->request->input('json_decode', true);
        $keyword = $post_decode['keyword'];
        if (!isset($post_decode['pageno']) || $post_decode['pageno'] <= 0) {
            $pageno = 1;
        } else {
            $pageno = $post_decode['pageno'];
        }
        if (isset($post_decode['other_user_id']) && $post_decode['other_user_id'] != '') {
            $other_user_id = $post_decode['other_user_id'];
        } else {
            $other_user_id = 0;
        }
//$limit = 10;
        $joins = array();
        $joins[0] = array(
            'table' => 'product_categories',
            'alias' => 'ProductCategory',
            'type' => 'inner',
            'conditions' => array('ProductCategory.product_id= Product2.id')
        );
        $joins[1] = array(
            'table' => 'categories',
            'alias' => 'Category',
            'type' => 'inner',
            'conditions' => array('Category.id =ProductCategory.category_id',)
        );

        $conditionsSubQuery['Category.name like'] = "%{$keyword}%";

        $db = $this->User->getDataSource();
        $subQuery = $db->buildStatement(
                array(
            'fields' => array('Product2.id'),
            'table' => $db->fullTableName($this->Product),
            'alias' => 'Product2',
            'limit' => null,
            'offset' => null,
            'joins' => $joins,
            'conditions' => $conditionsSubQuery,
            'order' => null,
            'group' => null
                ), $this->Product
        );
        $subQuery = ' Product.id  IN (' . $subQuery . ') ';
        $subQueryExpression = $db->expression($subQuery);

        $conditions['OR'][] = $subQueryExpression;
        $conditions['OR'][] = array('Product.prod_name like' => "%{$keyword}%");
        $this->Product->unbindModel(array('hasAndBelongsToMany' => array('Product')));
// $this->Product->unbindModel(array('hasAndBelongsToMany' => array('Category')));

        $this->Product->unbindModel(array('hasMany' => array('ProductImage')));
        $this->Product->bindModel(array('hasMany' => array('ProductImage' => array(
                    'className' => 'ProductImage',
                    'foreignKey' => 'product_id',
                    'dependent' => true,
                    'conditions' => 'ProductImage.addinroom=true',
                    'fields' => '',
                    'order' => '',
                    'limit' => '',
                    'offset' => '',
                    'exclusive' => '',
                    'finderQuery' => '',
                    'counterQuery' => ''
        ))));

        $this->Product->bindModel(array('hasOne' => array('UserFavorite' => array(
                    'className' => 'UserFavorite',
                    'foreignKey' => 'product_id',
                    'dependent' => true,
                    'conditions' => "UserFavorite.other_user_id={$other_user_id}",
                    'fields' => '',
                    'order' => '',
                    'limit' => '',
                    'offset' => '',
                    'exclusive' => '',
                    'finderQuery' => '',
                    'counterQuery' => ''
        ))));
        $product = $this->Product->find('all', array(
            'conditions' => $conditions,
            'recursive' => 1,
            'fields' => array('Product.id', 'Product.prod_name', 'Product.price', 'Product.available_units', 'UserFavorite.status')));



        $arr = array();
// print_R($product);exit;
        foreach ($product as $k => $p) {
            if ($product[$k]['Product']['price'] == null) {
                $product[$k]['Product']['price'] = '0.00';
            }

            if (isset($product[$k]['ProductImage'][0]['prod_img'])) {

                $sortArray = array();
                $people = $product[$k]['ProductImage'];
                foreach ($people as $person) {
                    foreach ($person as $key1 => $value) {
                        if (!isset($sortArray[$key1])) {
                            $sortArray[$key1] = array();
                        }
                        $sortArray[$key1][] = $value;
                    }
                }
                $orderby = "mainImage";
                array_multisort($sortArray[$orderby], SORT_DESC, $people);
                $product[$k]['ProductImage'] = $people;


                foreach ($product[$k]['ProductImage'] as $pi) {
                    if (strpos($pi['prod_img'], 'FRONT') !== false) {
                        $product[$k]['Product']['prod_img'] = $pi['prod_img'];
                        $product[$k]['Product']['prod_thumb'] = $pi['prod_thumb'];
                    }
                }
                if (!isset($product[$k]['Product']['prod_img'])) {
                    foreach ($product[$k]['ProductImage'] as $pi) {
                        if (strpos($pi['prod_img'], 'SIDE') !== false) {
                            $product[$k]['Product']['prod_img'] = $pi['prod_img'];
                            $product[$k]['Product']['prod_thumb'] = $pi['prod_thumb'];
                        }
                    }
                }
                if (!isset($product[$k]['Product']['prod_img'])) {
                    foreach ($product[$k]['ProductImage'] as $pi) {
                        if (strpos($pi['prod_img'], 'BACK') !== false) {
                            $product[$k]['Product']['prod_img'] = $pi['prod_img'];
                            $product[$k]['Product']['prod_thumb'] = $pi['prod_thumb'];
                        }
                    }
                }
                if (!isset($product[$k]['Product']['prod_img'])) {
                    $product[$k]['Product']['prod_img'] = $product[$k]['ProductImage'][0]['prod_img'];
                    $product[$k]['Product']['prod_thumb'] = $product[$k]['ProductImage'][0]['prod_thumb'];
                }
                if ($product[$k]['UserFavorite']['status'] == null) {
                    $product[$k]['Product']['favorite'] = 0;
                } else {
                    $product[$k]['Product']['favorite'] = 1;
                }
                $product[$k]['Product']['cat_id'] = $product[$k]['Category'][0]['id'];
                unset($product[$k]['ProductImage']);
                $arr[] = $product[$k]['Product'];
            }
        }


        if (!empty($product)) {
            echo $this->json_encode(array('status' => 'success', 'data' => $arr));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'data' => 'No products found.'));
        }
    }

    function getStyleCurations() {
        /* {"parent_id":1}   Optional */
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['parent_id']) && $post_decode['parent_id'] != '') {
            $parent_id = $post_decode['parent_id'];
            $conditions = array('StyleCuration.parent_id' => $parent_id);
        } else {
            $conditions = array('StyleCuration.parent_id' => 0);
        }
// print_R($conditions);
        $sc = $this->StyleCuration->find('all', array('conditions' => $conditions));
        if ($sc) {
            $arr = array();
            foreach ($sc as $k => $s) {
                $arr[$k]['id'] = $s['StyleCuration']['id'];
                $arr[$k]['style_curation_name'] = $s['StyleCuration']['style_curation_name'];
                $arr[$k]['style_curation_image'] = $s['StyleCuration']['style_curation_image'];
            }
            echo $this->json_encode(array('status' => 'success', 'data' => $arr));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => 'No style curation found'));
        }
    }

    function saveUserStyleCuration() {
        /* {"other_user_id":123,style_curation_id:"11"} */
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['other_user_id']) && isset($post_decode['style_curation_id'])) {
            $arr['UserStyleAction']['other_user_id'] = $post_decode['other_user_id'];
            $arr['UserStyleAction']['style_curation_id'] = $post_decode['style_curation_id'];
            $arr['UserStyleAction']['action_type'] = 1;
            if ($this->UserStyleAction->save($arr)) {
                echo $this->json_encode(array('status' => 'success', 'msg' => ''));
            } else {
                echo $this->json_encode(array('status' => 'fail', 'msg' => ''));
            }
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => ''));
        }
    }

    function getProductsById() {
        Configure::write('debug', 2);
        /* {"product_id":123} */
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['other_user_id']) && $post_decode['other_user_id'] != '') {
            $other_user_id = $post_decode['other_user_id'];
        } else {
            $other_user_id = 0;
        }
        if (isset($post_decode['product_id']) && $post_decode['product_id'] != '') {
            $product_id = $post_decode['product_id'];
            $this->Product->bindModel(array('hasOne' => array('UserFavorite' => array(
                        'className' => 'UserFavorite',
                        'foreignKey' => 'product_id',
                        'dependent' => true,
                        'conditions' => "UserFavorite.other_user_id={$other_user_id}",
                        'fields' => '',
                        'order' => '',
                        'limit' => '',
                        'offset' => '',
                        'exclusive' => '',
                        'finderQuery' => '',
                        'counterQuery' => ''
            ))));
//  $product = $this->Product->find('all',array('joins'=>$joins));
            $product = $this->Product->find('first', array('conditions' => array('Product.id' => $product_id), 'recursive' => 1));

            $sortArray = array();
            $people = $product['ProductImage'];
            foreach ($people as $person) {
                foreach ($person as $key => $value) {
                    if (!isset($sortArray[$key])) {
                        $sortArray[$key] = array();
                    }
                    $sortArray[$key][] = $value;
                }
            }
            $orderby = "mainImage";
            array_multisort($sortArray[$orderby], SORT_DESC, $people);
            $product['ProductImage'] = $people;


            if ($product['UserFavorite']['id'] == '') {
                $product['UserFavorite']['favorite'] = 0;
            } else {
                $product['UserFavorite']['favorite'] = 1;
            }
            $suggested = $this->getSuggestedProducts($post_decode['product_id']);
            if (!empty($suggested)) {
                $product['Suggested'] = $suggested;
            }
//print_R($product);
            $sql = "SELECT Product.id,ProductSpec.color, ProductSpec.dimensions,Product.`construction` FROM products AS Product"
                    . " LEFT JOIN `eyestylist`.`product_specs` AS `ProductSpec` ON (`ProductSpec`.`product_id` = `Product`.`id`)  "
                    . "inner JOIN (SELECT * FROM `product_images` WHERE (`mainImage`=1 OR addinroom=1) ORDER BY `mainImage` DESC ,`addinroom` DESC) AS ProductImage ON ProductImage.product_id=Product.id  "
                    . " WHERE parent_style_number = (SELECT parent_style_number FROM products WHERE  id ='{$product_id}') and parent_style_number<> '' "; //a

            $filters = $this->Product->query($sql);

            // exit;
            //print_R($filters);
            $arrFilterConstruction2 = $arrFilterD2 = $arrFilterConstruction = $arrFilterC = $arrFilterD = array();
            foreach ($filters AS $f) {
                $arrFilterC[$f['Product']['id']] = trim($f['ProductSpec']['color']);
                if ($f['ProductSpec']['dimensions'] != '') {
                    $arrFilterD[$f['Product']['id']] = $f['ProductSpec']['dimensions'];
                }
                $arrFilterConstruction[$f['Product']['id']] = $f['Product']['construction'];
            }

            $arrFilterC = array_filter(array_unique($arrFilterC));
            if (sizeof($arrFilterC) <= 0) {
                $arrFilterC = array();
            }

            $arrFilterD = array_filter(array_unique($arrFilterD));
            $c = $this->Category->getParents($product['Category'][0]['id']);
            if ($c['Category'] == '95' || $product['Category'][0]['id'] == '55') {
                
            } else {
                $arrFilterD = array();
            }
            if (sizeof($arrFilterD) <= 0) {
                $arrFilterD = array();
                $arrFilterD = array_filter(array_unique($arrFilterConstruction));
                if (sizeof($arrFilterD) <= 0) {
                    $arrFilterD = array();
                }
            }

            // print_R($arrFilterD);
            if (isset($post_decode['color']) && $post_decode['color'] != '') {
                $arrFilterD = array();
                foreach ($filters AS $f1) {
                    if (trim($f1['ProductSpec']['color']) == $post_decode['color']) {
                        if ($f1['ProductSpec']['dimensions'] != '') {
                            $arrFilterD2[$f1['Product']['id']] = $f1['ProductSpec']['dimensions'];
                        }
                        $arrFilterConstruction2[$f1['Product']['id']] = $f1['Product']['construction'];
                    }
                }
                $arrFilterD = array_filter(array_unique($arrFilterD2));
                $arrFilterD = array();
                if (sizeof($arrFilterD) <= 0) {
                    $arrFilterD = array();
                    $arrFilterD = array_filter(array_unique($arrFilterConstruction2));
                    if (sizeof($arrFilterD) <= 0) {
                        $arrFilterD = array();
                    }
                }
            }


            $product['variation'] = array('Color' => $arrFilterC, 'Dimensions' => $arrFilterD);
            echo $this->json_encode(array('status' => 'success', 'msg' => '', 'data' => $product));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => ''));
        }
    }

    function json_encode($data) {
        header('Content-Type: application/json');
        $data = json_encode($data);
        return str_replace('null', "\"\"", $data);
    }

    function saveUserFavorite() {
        /* {"other_user_id":123,product_id:"11",status:'1"} */
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['other_user_id']) && isset($post_decode['product_id'])) {
            $arr['UserFavorite']['other_user_id'] = $post_decode['other_user_id'];
            $arr['UserFavorite']['product_id'] = $post_decode['product_id'];
// if ($post_decode['status'] == 1) {
            $arr['UserFavorite']['status'] = 1;
// }
            $d = $this->UserFavorite->find('first', array('recursive' => -1, 'conditions' => array('UserFavorite.other_user_id' => $post_decode['other_user_id'], 'UserFavorite.product_id' => $post_decode['product_id'])));
//print_R($d);
            if ($d) {
                $this->UserFavorite->deleteAll(array('UserFavorite.other_user_id' => $post_decode['other_user_id'], 'UserFavorite.product_id' => $post_decode['product_id']));
            } else {
                $this->UserFavorite->save($arr);
            }
            $d3 = $this->UserFavorite->find('first', array('recursive' => -1, 'conditions' => array('UserFavorite.other_user_id' => $post_decode['other_user_id'], 'UserFavorite.product_id' => $post_decode['product_id'])));

            if ($d3) {
                echo $this->json_encode(array('status' => 'success', 'favorite' => array('status' => 1, 'user_id' => $post_decode['other_user_id'], 'product_id' => $post_decode['product_id'])));
            } else {
                echo $this->json_encode(array('status' => 'success', 'favorite' => array('status' => 0, 'user_id' => $post_decode['other_user_id'], 'product_id' => $post_decode['product_id'])));
            }
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => ''));
        }
    }

    function getSuggestedProducts($product_id = null) {
        $post_decode = $this->request->input('json_decode', true);

        $c = $this->ProductCategory->find('first', array('conditions' => array('ProductCategory.product_id' => $product_id), 'recursive' => -1));
        if ($c) {
            $category_id = $c['ProductCategory']['category_id'];
        } else {
            $category_id = 0;
        }


        $product = $this->Product->query(
                "SELECT `id`,null as more_products,`prod_name`,parent_style_number,`price`,`available_units`,`status`, prod_img,prod_thumb,category_order,style2_order,style3_order,style4_order,style5_order FROM ("
                . "SELECT `Product`.`id`, `Product`.`prod_name`, `Product`.`price`,Product.parent_style_number , `Product`.`available_units`,  `UserFavorite`.`status`,"
                . "ProductImage.prod_img,ProductImage.prod_thumb,category_order,style1_order,style2_order,style3_order,style4_order,style5_order FROM `eyestylist`.`products` AS `Product` "
                . "LEFT JOIN `eyestylist`.`product_shippings` AS `ProductShipping` ON (`ProductShipping`.`product_id` = `Product`.`id`) "
                . "LEFT JOIN `eyestylist`.`product_specs` AS `ProductSpec` ON (`ProductSpec`.`product_id` = `Product`.`id`) "
                . "LEFT JOIN `eyestylist`.`user_favorites` AS `UserFavorite` ON (`UserFavorite`.`other_user_id`=0 AND `UserFavorite`.`product_id` = `Product`.`id`) "
                . "INNER JOIN `eyestylist`.`product_categories` AS `ProductCategory` ON (`ProductCategory`.`product_id`= `Product`.`id`) "
                . "INNER JOIN `eyestylist`.`categories` AS `Category` ON (`Category`.`id` =`ProductCategory`.`category_id`) "
                . "inner JOIN (SELECT * FROM `product_images` WHERE (`mainImage`=1) ORDER BY `mainImage` DESC ,`addinroom` DESC) AS ProductImage ON ProductImage.product_id=Product.id  "
                . " WHERE `Category`.`id` IN ({$category_id}) and Product.id!='{$product_id}' GROUP BY ProductImage.`product_id`  ) AS Product   "
                . "   LIMIT 10 OFFSET 0 ");

        $arr = array();
        foreach ($product as $k => $p) {

            $arr[$k]['product_id'] = $product[$k]['Product']['id'];
            $arr[$k]['prod_name'] = $product[$k]['Product']['prod_name'];
            if (isset($product[$k]['Product']['prod_thumb'])) {
                $arr[$k]['prod_thumb'] = $product[$k]['Product']['prod_thumb'];
            }
        }


        return $arr;
        exit;

        $joins = array();
        $joins[0] = array(
            'table' => 'product_categories',
            'alias' => 'ProductCategory',
            'type' => 'inner',
            'conditions' => array('ProductCategory.product_id= Product.id')
        );
        $joins[1] = array(
            'table' => 'categories',
            'alias' => 'Category',
            'type' => 'inner',
            'conditions' => array('Category.id =ProductCategory.category_id',)
        );


        $this->Product->unbindModel(array('hasAndBelongsToMany' => array('Product')));
        $this->Product->unbindModel(array('hasAndBelongsToMany' => array('Category')));
//  $this->Product->unbindModel(array('hasMany' => array('ProductImage')));

        $this->Product->bindModel(array('hasMany' => array('ProductImage' => array(
                    'className' => 'ProductImage',
                    'foreignKey' => 'product_id',
                    'dependent' => true,
                    'conditions' => 'ProductImage.addinroom=true',
                    'fields' => '',
                    'order' => '',
                    'limit' => '1',
                    'offset' => '',
                    'exclusive' => '',
                    'finderQuery' => '',
                    'counterQuery' => ''
        ))));

        $product = $this->Product->find('all', array(
            'joins' => $joins,
            'conditions' => array('Category.id' => $category_id, 'Product.id !=' => $product_id),
            'recursive' => 1,
            'fields' => array('Product.id'),
            'limit' => 10)
        );
        $a = array();
        foreach ($product as $k => $p) {
            unset($product[$k]['Product']);
            $l = array();
            if (!empty($product[$k]['ProductImage'])) {
                $a[$k]['product_id'] = $product[$k]['ProductImage'][0]['product_id'];

                if (!empty($product[$k]['ProductImage'])) {
                    $sortArray = array();
                    $people = $product[$k]['ProductImage'];
                    foreach ($people as $person) {
                        foreach ($person as $key => $value) {
                            if (!isset($sortArray[$key])) {
                                $sortArray[$key] = array();
                            }
                            $sortArray[$key][] = $value;
                        }
                    }
                    $orderby = "mainImage";
                    array_multisort($sortArray[$orderby], SORT_DESC, $people);
                    $product[$k]['ProductImage'] = $people;
                }
                $a[$k]['prod_thumb'] = $product[$k]['ProductImage'][0]['prod_thumb'];

// $product[$k]['ProductImage'][$kp]['prod_name'] = $product[$k]['Product']['prod_name'];
// $product[$k]['ProductImage'][$kp]['cat_name'] = $product[$k]['Category']['name'];
// $product[$k]['ProductImage'][$kp]['cat_id'] = $product[$k]['Category']['id'];
//exit;
            }
            if (empty($product[$k]['ProductImage'])) {
                unset($product[$k]);
            }
        }
        sort($a);

        return $a;
    }

    function deleteCartItem() {
        $post_decode = $this->request->input('json_decode', true);
// print_R($post_decode);exit;
        if (isset($post_decode['id']) && $post_decode['id'] != '') {
            $this->CartItem->delete($post_decode['id']);
            echo $this->json_encode(array('status' => 'success', 'msg' => "Item deleted successfully"));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => ''));
        }
    }

    function addToCart() {
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['other_user_id']) && isset($post_decode['product_id'])) {
            $product = explode(",", $post_decode['product_id']);
            foreach ($product as $p) {
                $arr['CartItem']['other_user_id'] = $post_decode['other_user_id'];
                $arr['CartItem']['product_id'] = $p;
                $arr['CartItem']['quantity'] = $post_decode['quantity'];

                $d = $this->CartItem->find('first', array('recursive' => -1, 'conditions' => array('CartItem.other_user_id' => $post_decode['other_user_id'], 'CartItem.product_id' => $p)));
                if ($d) {
                    $arr['CartItem']['id'] = $d['CartItem']['id'];
                    $arr['CartItem']['quantity'] = $d['CartItem']['quantity'] + $arr['CartItem']['quantity'];
                } else {
                    $arr['CartItem']['created'] = date('Y-m-d H:i:s');
                    $this->CartItem->create();
                }
                $this->CartItem->save($arr);
            }

            $this->CartItem->virtualFields = array('subtotal' => 'CartItem.quantity * Product.price');
            $allCart = $this->CartItem->find('all', array('recursive' => 0, 'conditions' => array('CartItem.other_user_id' => $post_decode['other_user_id']),
                'fields' => array('CartItem.id', 'Product.id', 'Product.prod_name', 'Product.price', 'CartItem.quantity', 'subtotal')));
            $result = array();

            foreach ($allCart as $key => $cart) {
                $result[$key]['id'] = $cart['CartItem']['id'];
                $result[$key]['quantity'] = $cart['CartItem']['quantity'];
                $result[$key]['subtotal'] = number_format((float) $cart['CartItem']['subtotal'], 2);
                $result[$key]['product_id'] = $cart['Product']['id'];
                $result[$key]['prod_name'] = $cart['Product']['prod_name'];
            }

            echo $this->json_encode(array('status' => 'success', 'data' => $result));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => ''));
        }
    }

    function updateToCart() {
//{"other_user_id":5,"id":2,"quantity":11120}
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['other_user_id']) && isset($post_decode['id'])) {
            $arr['CartItem']['other_user_id'] = $post_decode['other_user_id'];
            $arr['CartItem']['id'] = $post_decode['id'];
            $arr['CartItem']['quantity'] = $post_decode['quantity'];


            $this->CartItem->save($arr);


            $arr['CartItem']['other_user_id'] = $post_decode['other_user_id'];
            $this->CartItem->virtualFields = array('subtotal' => 'CartItem.quantity * Product.price');
            $allCart = $this->CartItem->find('all', array('recursive' => 0, 'conditions' => array('CartItem.other_user_id' => $post_decode['other_user_id']),
                'fields' => array('CartItem.id', 'CartItem.created', 'Product.id', 'Product.prod_name', 'CartItem.quantity', 'subtotal')));
            $result = array();

            foreach ($allCart as $key => $cart) {
                $result[$key]['id'] = $cart['CartItem']['id'];
                $result[$key]['created'] = $cart['CartItem']['created'];
                $result[$key]['quantity'] = $cart['CartItem']['quantity'];
                $result[$key]['subtotal'] = number_format((float) $cart['CartItem']['subtotal'], 2);
                $result[$key]['product_id'] = $cart['Product']['id'];
                $result[$key]['prod_name'] = $cart['Product']['prod_name'];
                $img = $this->ProductImage->find('first', array('conditions' => array('ProductImage.product_id' => $cart['Product']['id'], 'ProductImage.mainImage' => 1)));

                if (!empty($img)) {
                    $result[$key]['img'] = $img['ProductImage']['prod_thumb'];
                } else {
                    $result[$key]['img'] = '';
                }
            }
            echo $this->json_encode(array('status' => 'success', 'data' => $result));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => ''));
        }
    }

    function viewCart() {
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['other_user_id'])) {
            $arr['CartItem']['other_user_id'] = $post_decode['other_user_id'];
            $this->CartItem->virtualFields = array('subtotal' => 'CartItem.quantity * Product.price');
            $allCart = $this->CartItem->find('all', array('recursive' => 0, 'conditions' => array('CartItem.other_user_id' => $post_decode['other_user_id']),
                'fields' => array('CartItem.id', 'CartItem.created', 'Product.id', 'Product.prod_name', 'CartItem.quantity', 'subtotal')));
            $result = array();

            foreach ($allCart as $key => $cart) {
                $result[$key]['id'] = $cart['CartItem']['id'];
                $result[$key]['created'] = $cart['CartItem']['created'];
                $result[$key]['quantity'] = $cart['CartItem']['quantity'];
                $result[$key]['subtotal'] = number_format((float) $cart['CartItem']['subtotal'], 2);
                $result[$key]['product_id'] = $cart['Product']['id'];
                $result[$key]['prod_name'] = $cart['Product']['prod_name'];
                $img = $this->ProductImage->find('first', array('conditions' => array('ProductImage.product_id' => $cart['Product']['id'], 'OR' => array(
                            array('ProductImage.mainImage' => 1),
                            array('ProductImage.addinroom' => 1),
                ))));

                if (!empty($img)) {
                    $result[$key]['img'] = $img['ProductImage']['prod_thumb'];
                } else {
                    $result[$key]['img'] = '';
                }
            }
            echo $this->json_encode(array('status' => 'success', 'data' => $result));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => ''));
        }
    }

    function getQuiz() {
        $Stylequiz = $this->Stylequiz->find('all');
        $arr = array();
        foreach ($Stylequiz as $s) {
            $arr[$s['Stylequiz']['style_type']]['title'] = $s['Stylequiz']['title_text'];
            if ($s['Stylequiz']['is_image'] == true) {

                $arr[$s['Stylequiz']['style_type']]['data'][] = $s['Stylequiz']['curation_images'];
            } else {
                $arr[$s['Stylequiz']['style_type']]['data'][] = $s['Stylequiz']['curation_text'];
            }
        }
        echo $this->json_encode(array('status' => 'success', 'data' => $arr));
    }

    function getProductsFavorite() {
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['cat_id']) && $post_decode['cat_id'] != '') {
            $category_id = $post_decode['cat_id'];
        } else {
            $category_id = null;
        }
        if (isset($post_decode['other_user_id']) && $post_decode['other_user_id'] != '') {
            $other_user_id = $post_decode['other_user_id'];
        } else {
            $other_user_id = 0;
        }


        $joins = array();
        $joins[0] = array(
            'table' => 'product_categories',
            'alias' => 'ProductCategory',
            'type' => 'inner',
            'conditions' => array('ProductCategory.product_id= Product.id')
        );
        $joins[1] = array(
            'table' => 'categories',
            'alias' => 'Category',
            'type' => 'inner',
            'conditions' => array('Category.id =ProductCategory.category_id')
        );


        $this->Product->unbindModel(array('hasAndBelongsToMany' => array('Product')));
        $this->Product->unbindModel(array('hasAndBelongsToMany' => array('Category')));
        /* $this->Product->unbindModel(array('hasMany' => array('ProductImage')));  */


        $this->Product->bindModel(array('hasMany' => array('ProductImage' => array(
                    'className' => 'ProductImage',
                    'foreignKey' => 'product_id',
                    'dependent' => true,
                    'conditions' => 'ProductImage.addinroom=true',
                    'fields' => '',
                    'order' => '',
                    'limit' => '',
                    'offset' => '',
                    'exclusive' => '',
                    'finderQuery' => '',
                    'counterQuery' => ''
        ))));

        $this->Product->bindModel(array('hasOne' => array('UserFavorite' => array(
                    'className' => 'UserFavorite',
                    'foreignKey' => 'product_id',
                    'dependent' => true,
                    'conditions' => "UserFavorite.other_user_id={$other_user_id}",
                    'fields' => '',
                    'order' => '',
                    'limit' => '',
                    'offset' => '',
                    'exclusive' => '',
                    'finderQuery' => '',
                    'counterQuery' => ''
        ))));
        $product2 = $this->Product->find('all', array(
            'joins' => $joins,
            //'conditions' => array('Category.id' => $category_id),
            'conditions' => array('UserFavorite.status' => 1),
            'order' => 'UserFavorite.id desc',
            'recursive' => 1,
            'fields' => array('UserFavorite.id', 'Product.id', 'Product.prod_name', 'Product.price', 'Product.available_units', 'Category.id', 'UserFavorite.status'))
        );

        //Configure::write('debug', 2);
        $product = $this->Product->query(
                "SELECT `id`,COUNT(`id`) as more_products, cat_id,`prod_name`,parent_style_number,`price`,`available_units`,`status`, prod_img,prod_thumb FROM ("
                . "SELECT `Product`.`id`, `Product`.`prod_name`, `Category`.`id` as cat_id,`Product`.`price`,Product.parent_style_number , `Product`.`available_units`,  `UserFavorite`.`status`,"
                . "ProductImage.prod_img,ProductImage.prod_thumb FROM `eyestylist`.`products` AS `Product` "
                . "LEFT JOIN `eyestylist`.`product_shippings` AS `ProductShipping` ON (`ProductShipping`.`product_id` = `Product`.`id`) "
                . "LEFT JOIN `eyestylist`.`product_specs` AS `ProductSpec` ON (`ProductSpec`.`product_id` = `Product`.`id`) "
                . "LEFT JOIN `eyestylist`.`user_favorites` AS `UserFavorite` ON (`UserFavorite`.`product_id` = `Product`.`id`) "
                . "INNER JOIN `eyestylist`.`product_categories` AS `ProductCategory` ON (`ProductCategory`.`product_id`= `Product`.`id`) "
                . "INNER JOIN `eyestylist`.`categories` AS `Category` ON (`Category`.`id` =`ProductCategory`.`category_id`) "
                . "inner JOIN (SELECT * FROM `product_images` WHERE (`mainImage`=1 OR addinroom=1) ORDER BY `mainImage` DESC ,`addinroom` DESC) AS ProductImage ON ProductImage.product_id=Product.id  "
                . " WHERE UserFavorite.status=1 and UserFavorite.other_user_id={$other_user_id} GROUP BY ProductImage.`product_id` order by  UserFavorite.id desc ) AS Product"
                . " GROUP BY parent_style_number");

        //   print_R($product);

        $arr = array();
        foreach ($product as $k => $p) {
            if ($product[$k]['Product']['price'] == null) {
                $product[$k]['Product']['price'] = '0.00';
            }
            $arr[$k]['id'] = $product[$k]['Product']['id'];
            $arr[$k]['prod_name'] = $product[$k]['Product']['prod_name'];
            $arr[$k]['price'] = $product[$k]['Product']['price'];
            $arr[$k]['available_units'] = $product[$k]['Product']['available_units'];
            $s = $this->Product->query("Select Count(Product.id) as more_products from products as Product "
                    . "inner JOIN (SELECT * FROM `product_images` WHERE (`mainImage`=1 OR addinroom=1) ORDER BY `mainImage` DESC ,`addinroom` DESC) AS ProductImage ON ProductImage.product_id=Product.id  "
                    . "where parent_style_number='{$product[$k]['Product']['parent_style_number']}'");
            // print_R($s[0][0]['more_products']);
            $arr[$k]['more_products'] = $s[0][0]['more_products'];
            $arr[$k]['cat_id'] = $product[$k]['Product']['cat_id'];
            if (isset($product[$k]['Product']['prod_img'])) {
                $arr[$k]['prod_img'] = $product[$k]['Product']['prod_img'];
                $arr[$k]['prod_thumb'] = $product[$k]['Product']['prod_thumb'];

                if ($product[$k]['Product']['status'] == null) {
                    $arr[$k]['favorite'] = 0;
                } else {
                    $arr[$k]['favorite'] = 1;
                }
                unset($product[$k]['ProductImage']);
                unset($product[$k]['Product']);
            }
        }

//        $arr = array();
//        foreach ($product as $k => $p) {
//            if ($product[$k]['Product']['price'] == null) {
//                $product[$k]['Product']['price'] = '0.00';
//            }
//            if (isset($product[$k]['ProductImage'][0]['prod_img'])) {
//
//                $sortArray = array();
//                $people = $product[$k]['ProductImage'];
//                foreach ($people as $person) {
//                    foreach ($person as $key1 => $value) {
//                        if (!isset($sortArray[$key1])) {
//                            $sortArray[$key1] = array();
//                        }
//                        $sortArray[$key1][] = $value;
//                    }
//                }
//                $orderby = "mainImage";
//                array_multisort($sortArray[$orderby], SORT_DESC, $people);
//                $product[$k]['ProductImage'] = $people;
//
//                foreach ($product[$k]['ProductImage'] as $pi) {
//                    if (strpos($pi['prod_img'], 'FRONT') !== false) {
//                        $product[$k]['Product']['prod_img'] = $pi['prod_img'];
//                        $product[$k]['Product']['prod_thumb'] = $pi['prod_thumb'];
//                    }
//                }
//                if (!isset($product[$k]['Product']['prod_img'])) {
//                    foreach ($product[$k]['ProductImage'] as $pi) {
//                        if (strpos($pi['prod_img'], 'SIDE') !== false) {
//                            $product[$k]['Product']['prod_img'] = $pi['prod_img'];
//                            $product[$k]['Product']['prod_thumb'] = $pi['prod_thumb'];
//                        }
//                    }
//                }
//                if (!isset($product[$k]['Product']['prod_img'])) {
//                    foreach ($product[$k]['ProductImage'] as $pi) {
//                        if (strpos($pi['prod_img'], 'BACK') !== false) {
//                            $product[$k]['Product']['prod_img'] = $pi['prod_img'];
//                            $product[$k]['Product']['prod_thumb'] = $pi['prod_thumb'];
//                        }
//                    }
//                }
//                if (!isset($product[$k]['Product']['prod_img'])) {
//                    $product[$k]['Product']['prod_img'] = $product[$k]['ProductImage'][0]['prod_img'];
//                    $product[$k]['Product']['prod_thumb'] = $product[$k]['ProductImage'][0]['prod_thumb'];
//                }
//                if ($product[$k]['UserFavorite']['status'] == null) {
//                    $product[$k]['Product']['favorite'] = 0;
//                } else {
//                    $product[$k]['Product']['favorite'] = 1;
//                }
//                $product[$k]['Product']['cat_id'] = $product[$k]['Category']['id'];
//                unset($product[$k]['ProductImage']);
//                $arr[] = $product[$k]['Product'];
//            }
//        }

        if (!empty($arr)) {
            echo $this->json_encode(array('status' => 'success', 'data' => $arr));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'data' => 'No products found.'));
        }
    }

    function whatsnew_old() {
        $quiz = $this->ProductWhat->find('all', array('order' => 'ProductWhat.id asc', 'recursive' => 1));
        $quiz2 = $this->ProductWhatSlider->find('all', array('order' => 'orders,ProductWhatSlider.id asc', 'recursive' => -1, 'fields' => 'category_id,image,orders'));
        $arr = array();
        if ($quiz) {
            foreach ($quiz as $key => $c) {
                $arr[$key]['ProductWhat']['id'] = $c['ProductWhat']['id'];
                $arr[$key]['ProductWhat']['Category_id'] = $c['Category']['id'];
                $arr[$key]['ProductWhat']['Category_name'] = $c['Category']['name'];
                $arr[$key]['ProductWhat']['display_name'] = $c['ProductWhat']['display_name'];
                $arr[$key]['ProductWhat']['Product_name'] = $c['Product']['prod_name'];
                $arr[$key]['ProductWhat']['Product_id'] = $c['Product']['id'];
                $img = $this->ProductImage->find('first', array('conditions' => array('ProductImage.product_id' => $c['Product']['id'])));
                if (!empty($img)) {
                    $arr[$key]['ProductWhat']['Product_img'] = $img['ProductImage']['prod_thumb'];
                } else {
                    $arr[$key]['ProductWhat']['Product_img'] = '';
                }
            }
            echo $this->json_encode(array('status' => 'success', 'data' => $arr, 'slider' => $quiz2));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'message' => 'No new product found'));
        }
    }

    function whatsnew() {
        $post_decode = $this->request->input('json_decode', true);
        $styles = $this->UserStyle->find('list', array('conditions' => array('UserStyle.other_user_id' => $post_decode['other_user_id'], 'UserStyle.primary_style' => 1), 'fields' => array('UserStyle.style_id')));

        if ($styles) {
            $quiz = $this->ProductWhat->find('all', array('order' => 'ProductWhat.orders asc', 'recursive' => 1, 'conditions' => array('ProductWhat.styles' => $styles)));
            if (!$quiz) {
                $styles = $this->UserStyle->find('list', array('conditions' => array('UserStyle.other_user_id' => $post_decode['other_user_id'], 'UserStyle.primary_style' => 0), 'fields' => array('UserStyle.style_id')));
                $quiz = $this->ProductWhat->find('all', array('order' => 'ProductWhat.orders asc', 'recursive' => 1, 'conditions' => array('ProductWhat.styles' => $styles)));
            }
        } else {
            $quiz = $this->ProductWhat->find('all', array('order' => 'ProductWhat.orders asc', 'recursive' => 1, 'group' => 'category_id'));
            //print_R($quiz);exit;
        }
        $quiz2 = $this->ProductWhatSlider->find('all', array('order' => 'orders,ProductWhatSlider.id asc', 'recursive' => -1,
            'fields' => 'category_id,image,orders,ProductWhatSlider.id'));
        $arr = array();
        if ($quiz) {
            foreach ($quiz as $key => $c) {
                $arr[$key]['ProductWhat']['id'] = $c['ProductWhat']['id'];
                $arr[$key]['ProductWhat']['display_name'] = $c['ProductWhat']['display_name'];
                $arr[$key]['ProductWhat']['Category_id'] = $c['Category']['id'];
                $arr[$key]['ProductWhat']['Category_name'] = $c['Category']['name'];
                $arr[$key]['ProductWhat']['Product_name'] = '';
                $arr[$key]['ProductWhat']['Product_id'] = '';
                $arr[$key]['ProductWhat']['Product_img'] = $c['ProductWhat']['image'];

                $style = $this->Style->find('first', array('conditions' => array('Style.id' => $c['ProductWhat']['styles']), 'recursive' => -1));

                $arr[$key]['ProductWhat']['style_id'] = $c['ProductWhat']['styles'];
                $arr[$key]['ProductWhat']['style_name'] = (isset($style['Style']['name']) && $style['Style']['name'] != '') ? $style['Style']['name'] : '';
                $arr[$key]['ProductWhat']['direct'] = ($c['ProductWhat']['direct'] == 1) ? 1 : 0;
                $arr[$key]['ProductWhat']['show_categories'] = ($c['ProductWhat']['show_categories'] == 1) ? 1 : 0;
            }

            $count = array();
            if (isset($post_decode['other_user_id']) && $post_decode['other_user_id'] != '') {
                $designboard = $this->UserDesignBoard->find('count', array('recursive' => -1,
                    'conditions' => array('UserDesignBoard.other_user_id' => $post_decode['other_user_id'])
                ));
                // $this->CartItem->virtualFields = array('subtotal' => 'SUM(CartItem.quantity)');
                //  $Cart = $this->CartItem->find('count', array('recursive' => 0, 'conditions' => array('CartItem.other_user_id' => $post_decode['other_user_id']), 'fields' => array('subtotal'), 'group' => 'CartItem.product_id'
                //  ));
                $this->CartItem->virtualFields = array('subtotal' => 'SUM(CartItem.quantity)');
                $Cart = $this->CartItem->find('first', array('recursive' => 0, 'conditions' => array('CartItem.other_user_id' => $post_decode['other_user_id']), 'fields' => array('subtotal')
                ));

                $current_styles = $this->UserStyle->find('first', array('conditions' => array('UserStyle.other_user_id' => $post_decode['other_user_id'], 'UserStyle.primary_style' => 1), 'fields' => array('Style.name')));
                if ($current_styles) {
                    $current_style = $current_styles['Style']['name'];
                } else {
                    $current_style = "";
                }
                $count = array('designboard' => $designboard, 'cart' => (int) $Cart['CartItem']['subtotal'], 'current_style' => $current_style);
            }


            echo $this->json_encode(array('status' => 'success', 'data' => $arr, 'slider' => $quiz2, 'count' => $count));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'message' => 'No new product found'));
        }
    }

    function saveUserDesignBoard() {
        Configure::write('debug', 0);

        /* {"other_user_id":123,product_id:"11",status:'1"} */
//$post_decode = $this->request->input('json_decode', true);
        header('Content-type: application/json');
        $post_decode = $this->request->data;

        if (isset($post_decode['other_user_id']) && isset($post_decode['product_id'])) {
            $arr['UserDesignBoard']['other_user_id'] = $post_decode['other_user_id'];
            $arr['UserDesignBoard']['product_id'] = $post_decode['product_id'];

            $product_id = explode(",", $post_decode['product_id']);

            $product = $this->Product->find('first', array(
                'conditions' => array('Product.id' => $product_id[0]),
                'fields' => array('Product.prod_name', 'Product.price'))
            );

            $arr['UserDesignBoard']['name'] = $product['Product']['prod_name'];
// $arr['UserDesignBoard']['price'] = $product['Product']['price'];
            $arr['UserDesignBoard']['dt'] = date('Y-m-d H:i:s', time()); //2016-01-25 12:13:59

            App::uses('S3', 'Vendor');
            $bucket = "eyestylist";
            if (!class_exists('S3'))
                require_once('S3.php');
            if (!defined('awsAccessKey'))
                define('awsAccessKey', 'AKIAIZLIIN2ROT4M64CQ');
            if (!defined('awsSecretKey'))
                define('awsSecretKey', '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');
            $s3 = new S3(awsAccessKey, awsSecretKey);
            $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
            $file = "DesignBoard_" . time() . '_' . $this->request->params['form']['image']['name'];
            $s3->putObjectFile($this->request->params['form']['image']['tmp_name'], $bucket, $file, S3::ACL_PUBLIC_READ);
            $file_name = 'http://' . $bucket . '.s3.amazonaws.com/' . $file;
            $arr['UserDesignBoard']['image'] = $file_name;
            $arr['UserDesignBoard']['status'] = 1;

            /* $d = $this->UserDesignBoard->find('first', array('recursive' => -1, 'conditions' => array('UserDesignBoard.other_user_id' => $post_decode['other_user_id'], 'UserDesignBoard.product_id' => $post_decode['product_id'])));

              if ($d) {
              $arr['UserDesignBoard']['id'] = $d['UserDesignBoard']['id'];
              // $this->UserDesignBoard->deleteAll(array('UserDesignBoard.other_user_id' => $post_decode['other_user_id'], 'UserDesignBoard.product_id' => $post_decode['product_id']));
              } else {

              } */
            $this->UserDesignBoard->save($arr);
            $d3 = $this->UserDesignBoard->find('first', array('recursive' => -1, 'conditions' => array('UserDesignBoard.other_user_id' => $post_decode['other_user_id'], 'UserDesignBoard.product_id' => $post_decode['product_id'])));

            /* if ($d3) {
              echo $this->json_encode(array('status' => 'success', 'data' => array('user_id' => $post_decode['other_user_id'], 'product_id' => $post_decode['product_id'])));
              } else {
              echo $this->json_encode(array('status' => 'success', 'data' => array('user_id' => $post_decode['other_user_id'], 'product_id' => $post_decode['product_id'])));
              } */
            echo $this->json_encode(array('status' => 'success', 'data' => array('user_id' => $post_decode['other_user_id'], 'product_id' => $post_decode['product_id'])));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => ''));
        }
        exit;
    }

    function deleteDesignBoard() {
        $post_decode = $this->request->input('json_decode', true);
// print_R($post_decode);exit;
        if (isset($post_decode['id']) && $post_decode['id'] != '') {
            App::uses('S3', 'Vendor');
            $bucket = "eyestylist";
            if (!class_exists('S3'))
                require_once('S3.php');
            if (!defined('awsAccessKey'))
                define('awsAccessKey', 'AKIAIZLIIN2ROT4M64CQ');
            if (!defined('awsSecretKey'))
                define('awsSecretKey', '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');
            $s3 = new S3(awsAccessKey, awsSecretKey);
            $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
            $product = $this->UserDesignBoard->find('first', array('conditions' => array('UserDesignBoard.id' => $post_decode['id'])));
            $s3->deleteObject($bucket, baseName($product['UserDesignBoard']['image']));


            $this->UserDesignBoard->delete($post_decode['id']);
            echo $this->json_encode(array('status' => 'success', 'msg' => "Item deleted successfully"));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => ''));
        }
    }

    function getDesignBoard() {
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['other_user_id']) && $post_decode['other_user_id'] != '') {
            $db = $this->UserDesignBoard->find('all', array('recursive' => -1,
                'conditions' => array('UserDesignBoard.other_user_id' => $post_decode['other_user_id']),
                'order' => array('UserDesignBoard.dt desc'),
            ));
            if ($db) {
                $arr = array();
                foreach ($db as $k => $c) {
                    $products_id = explode(",", $c['UserDesignBoard']['product_id']);
                    $products = $this->Product->find('all', array('conditions' => array('Product.id' => $products_id), 'recursive' => -1, 'fields' => array('Product.prod_name', 'Product.price', 'Product.id')));
                    $p = array();
                    foreach ($products as $product) {
                        $productsi = $this->ProductImage->find('first', array('conditions' => array('ProductImage.product_id' => $product['Product']['id'], 'OR' => array(
                                    array('ProductImage.addinroom' => 0),
                                    array('ProductImage.addinroom' => 1),
                                )), 'recursive' => -1, 'fields' => array('ProductImage.prod_thumb'), 'order' => array('ProductImage.mainimage desc')));
                        $product['Product']['prod_thumb'] = @$productsi['ProductImage']['prod_thumb'];
                        $p[] = $product['Product'];
                    }
//  print_R($p);
                    $c['UserDesignBoard']['products'] = $p;
                    $arr[] = $c['UserDesignBoard'];
                }

                echo $this->json_encode(array('status' => 'success', 'data' => $arr));
            } else {
                echo $this->json_encode(array('status' => 'success', 'msg' => 'No record found!'));
            }
        } else {
            echo $this->json_encode(array('status' => 'success', 'msg' => 'No record found!'));
        }
    }

    function getFeaturedProducts() {
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['cat_id']) && $post_decode['cat_id'] != '') {
            $category_id = $post_decode['cat_id'];
        } else {
            $category_id = null;
        }
        if (isset($post_decode['page']) && $post_decode['page'] != '') {
            $page = $post_decode['page'];
        } else {
            $page = 1;
        }

        $joins = array();
        $joins[0] = array(
            'table' => 'product_categories',
            'alias' => 'ProductCategory',
            'type' => 'inner',
            'conditions' => array('ProductCategory.product_id= FeaturedProduct.product_id')
        );
        $joins[1] = array(
            'table' => 'categories',
            'alias' => 'Category',
            'type' => 'inner',
            'conditions' => array('Category.id =ProductCategory.category_id',)
        );


        $this->Product->unbindModel(array('hasAndBelongsToMany' => array('Category')));
// $this->Product->unbindModel(array('hasMany' => array('ProductImage')));

        $this->Product->bindModel(array('hasMany' => array('ProductImage' => array(
                    'className' => 'ProductImage',
                    'foreignKey' => 'product_id',
                    'dependent' => true,
                    'conditions' => 'ProductImage.addinroom=1',
                    'fields' => '',
                    'order' => '',
                    'limit' => '',
                    'offset' => '',
                    'exclusive' => '',
                    'finderQuery' => '',
                    'counterQuery' => ''
        ))));
        $limit = 100;
        if (isset($post_decode['offset']) && $post_decode['offset'] != '') {
            $offset = $post_decode['offset'];
        } else {
            $offset = 0;
        }
        $product = $this->FeaturedProduct->find('all', array(
            'joins' => $joins,
            //'conditions' => array('Category.id' => $category_id),
            'recursive' => 2,
            'fields' => array('Product.id', 'Product.prod_name', 'Category.name', 'Category.id'),
            'order' => 'orders asc',
            'limit' => $limit,
            'offset' => $offset
                )
        );
        $j = array();
        foreach ($product as $k => $p) {
            if (empty($product[$k]['Product']['Product']['ProductImage'])) {
                unset($product[$k]);
            }
            if (!empty($product[$k]['Product']['Product']['ProductImage'])) {
                foreach ($product[$k]['Product']['Product']['ProductImage'] as $kp => $pi) {
// print_R($product[$k]['Product']['Product']);
                    $j[$k]['ProductImage'][$kp] = $pi;
                    $j[$k]['ProductImage'][$kp]['prod_name'] = $product[$k]['Product']['Product']['prod_name'];
                    $j[$k]['ProductImage'][$kp]['cat_name'] = $product[$k]['Category']['name'];
                    $j[$k]['ProductImage'][$kp]['cat_id'] = $product[$k]['Category']['id'];
                    $c = $this->Category->getParents($product[$k]['Category']['id']);
                    $j[$k]['ProductImage'][$kp]['parent_cat_id'] = $c['Category'];
                    $j[$k]['ProductImage'][$kp]['dimensions'] = $product[$k]['Product']['Product']['ProductSpec']['dimensions'];

// print_R( $c['Category']);exit;
                } //print_R($product[$k]['ProductImage']);
                $sortArray = array();
                $people = $j[$k]['ProductImage'];
                foreach ($people as $person) {
                    foreach ($person as $key => $value) {
                        if (!isset($sortArray[$key])) {
                            $sortArray[$key] = array();
                        }
                        $sortArray[$key][] = $value;
                    }
                }
                $orderby = "mainImage";
                array_multisort($sortArray[$orderby], SORT_DESC, $people);
                $j[$k]['ProductImage'] = $people;
//exit;
            }


//unset($product[$k]['ProductSpec']);
        }
        unset($product);
        unset($c);
        sort($j);
// print_R($j);
// exit;
        if (!empty($j)) {
            echo $this->json_encode(array('status' => 'success', 'data' => $j, 'offset' => $offset + 100));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'data' => 'No products found.'));
        }
    }

    function getRotateImages() {
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['product_id']) && $post_decode['product_id'] != '') {
            if ($post_decode['img_id'] == '') {
                $post_decode['img_id'] = 0;
            }

            $images = $this->ProductImage->find('all', array('fields' => array('ProductImage.product_id', 'ProductImage.prod_img', 'ProductImage.prod_thumb', 'ProductImage.id'), 'conditions' => array('ProductImage.product_id' => $post_decode['product_id'], 'ProductImage.id !=' => $post_decode['img_id'], 'ProductImage.addinroom' => true), 'recursive' => -1));

            if ($images) {
                $final_images = array();
                foreach ($images as $image) {
                    $final_images[] = $image['ProductImage'];
                }

                echo $this->json_encode(array('status' => 'success', 'data' => $final_images));
            } else {
                echo $this->json_encode(array('status' => 'fail', 'data' => 'No more images'));
            }
        } else {
            echo $this->json_encode(array('status' => 'fail', 'data' => 'some thing wrong'));
        }
    }

    function deleteShippingAddress() {
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['other_user_id']) && $post_decode['other_user_id'] != '' && $post_decode['id'] != '' && isset($post_decode['id'])) {
            $this->OtherUserShippingAdd->id = $post_decode['id'];
            $this->OtherUserShippingAdd->saveField('status', 0);
            echo $this->json_encode(array('status' => 'success', 'msg' => "Shipping Address deleted successfully"));
        } else {
            echo $this->json_encode(array('status' => 'failed', 'msg' => "Please provide Shipping address id and user id"));
        }
    }

    function addShippingAddress() {
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['other_user_id']) && $post_decode['other_user_id'] != '') {
            $arr = array();
            $arr['OtherUserShippingAdd']['other_user_id'] = $post_decode['other_user_id'];
            $arr['OtherUserShippingAdd']['address_name'] = $post_decode['address_name'];
            $arr['OtherUserShippingAdd']['email'] = $post_decode['email'];
            $arr['OtherUserShippingAdd']['full_name'] = $post_decode['full_name'];
            $arr['OtherUserShippingAdd']['zipcode'] = $post_decode['zipcode'];
            $arr['OtherUserShippingAdd']['city'] = $post_decode['city'];
            $arr['OtherUserShippingAdd']['state'] = $post_decode['state'];
            $arr['OtherUserShippingAdd']['address'] = $post_decode['address'];
            $arr['OtherUserShippingAdd']['phone'] = $post_decode['phone'];
            $arr['OtherUserShippingAdd']['dt'] = date("Y-m-d H:i:s", time());
            $arr['OtherUserShippingAdd']['detault_address'] = 0;
            $this->OtherUserShippingAdd->create();
            $this->OtherUserShippingAdd->save($arr);
            echo $this->json_encode(array('status' => 'success', 'msg' => "Shipping address added successfully", 'id' => $this->OtherUserShippingAdd->id));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => "Shipping address not added"));
        }
    }

    function editShippingAddress() {
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['other_user_id']) && $post_decode['other_user_id'] != '') {
            $arr = array();
            $arr['OtherUserShippingAdd']['id'] = $post_decode['id'];
            $arr['OtherUserShippingAdd']['other_user_id'] = $post_decode['other_user_id'];
            $arr['OtherUserShippingAdd']['address_name'] = $post_decode['address_name'];
            $arr['OtherUserShippingAdd']['email'] = $post_decode['email'];
            $arr['OtherUserShippingAdd']['full_name'] = $post_decode['full_name'];
            $arr['OtherUserShippingAdd']['zipcode'] = $post_decode['zipcode'];
            $arr['OtherUserShippingAdd']['city'] = $post_decode['city'];
            $arr['OtherUserShippingAdd']['state'] = $post_decode['state'];
            $arr['OtherUserShippingAdd']['address'] = $post_decode['address'];
            $arr['OtherUserShippingAdd']['phone'] = $post_decode['phone'];
            $arr['OtherUserShippingAdd']['dt'] = date("Y-m-d H:i:s", time());
            $arr['OtherUserShippingAdd']['detault_address'] = 0;
            $this->OtherUserShippingAdd->save($arr);
            echo $this->json_encode(array('status' => 'success', 'msg' => "Shipping address updated successfully", 'id' => $post_decode['id']));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => "Shipping address not added"));
        }
    }

    function getShippingAddresses() {
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['other_user_id']) && $post_decode['other_user_id'] != '') {
            $arr = array();

            $ss = $this->OtherUserShippingAdd->find('all', array('conditions' => array('OtherUserShippingAdd.other_user_id' => $post_decode['other_user_id'], 'OtherUserShippingAdd.status' => 1)));
            if ($ss) {
                foreach ($ss as $key => $s) {
                    $arr[$key]['other_user_id'] = $s['OtherUserShippingAdd']['other_user_id'];
                    $arr[$key]['address_name'] = $s['OtherUserShippingAdd']['address_name'];
                    $arr[$key]['email'] = $s['OtherUserShippingAdd']['email'];
                    $arr[$key]['full_name'] = $s['OtherUserShippingAdd']['full_name'];
                    $arr[$key]['zipcode'] = $s['OtherUserShippingAdd']['zipcode'];
                    $arr[$key]['city'] = $s['OtherUserShippingAdd']['city'];
                    $arr[$key]['state'] = $s['OtherUserShippingAdd']['state'];
                    $arr[$key]['address'] = $s['OtherUserShippingAdd']['address'];
                    $arr[$key]['dt'] = $s['OtherUserShippingAdd']['dt'];
                    $arr[$key]['detault_address'] = $s['OtherUserShippingAdd']['detault_address'];
                    $arr[$key]['phone'] = $s['OtherUserShippingAdd']['phone'];
                    $arr[$key]['id'] = $s['OtherUserShippingAdd']['id'];
                }
                echo $this->json_encode(array('status' => 'success', 'data' => $arr));
            } else {
                echo $this->json_encode(array('status' => 'fail', 'msg' => "No Shipping address available"));
            }
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => "Error"));
        }
    }

    function getStyleOptions() {
        $post_decode = $this->request->input('json_decode', true);
        $Style = $this->StyleOption->find('all', array('recursive' => -1));
        $arr = array();
        foreach ($Style as $key => $st) {
            $arr[$key]['style_id'] = $st['StyleOption']['style_id'];
            $arr[$key]['name'] = $st['StyleOption']['name'];
        }
        echo $this->json_encode(array('status' => 'success', 'data' => $arr));
    }

    function getStyleImages() {
        $post_decode = $this->request->input('json_decode', true);
        if (!empty($post_decode['style_id'])) {
            $arr = array();
            $i = 0;

            foreach ($post_decode['style_id'] as $style_id) {
                $Style = $this->StyleImage->find('all', array('conditions' => array('StyleImage.style_id' => $style_id), 'recursive' => -1, 'limit' => rand(2, 3)));
                foreach ($Style as $s) {
                    $arr[$i]['style_id'] = $s['StyleImage']['style_id'];
                    $arr[$i]['images'] = $s['StyleImage']['images'];
                    $i++;
                }
            }
            if (count($post_decode['style_id']) <= 1) {
                $Style = $this->StyleImage->find('all', array('recursive' => -1, 'order' => 'rand()', 'limit' => rand(2, 3), 'conditions' => array('StyleImage.style_id !=' => $style_id)));
                foreach ($Style as $s1) {
                    $arr[$i]['style_id'] = $s1['StyleImage']['style_id'];
                    $arr[$i]['images'] = $s1['StyleImage']['images'];
                    $i++;
                }
            }
            echo $this->json_encode(array('status' => 'success', 'data' => $arr));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => "Please provide style id"));
        }
    }

    function saveUserStyles() {
        $post_decode = $this->request->input('json_decode', true);
        if (!empty($post_decode['style_id'])) {
            $styles = explode(',', $post_decode['style_id']);
            $user = $post_decode['other_user_id'];
            $this->UserStyle->deleteAll(array('other_user_id' => $user));
            foreach ($styles as $k => $style) {
                $arr = array();
                $this->UserStyle->create();
                $arr['UserStyle']['other_user_id'] = $user;
                $arr['UserStyle']['style_id'] = $style;
                if ($k == 0) {
                    $arr['UserStyle']['primary_style'] = 1;
                } else {
                    $arr['UserStyle']['primary_style'] = 0;
                }
                $this->UserStyle->save($arr);
            }
            echo $this->json_encode(array('status' => 'success'));
        } else {
            echo $this->json_encode(array('status' => 'fail'));
        }
    }

    function getFilters() {
        $arr = array();
        $Style = $this->Style->find('all', array('order' => 'name asc', 'recursive' => -1));
        $sty = array();
        $arr['Price'] = array('Low to High', 'High to Low');
        foreach ($Style as $key => $s) {
// $sty[$key]['style_id'] = $s['Style']['style'];
            $sty[$s['Style']['style']] = $s['Style']['name'];
        }
        $arr['Styles'] = $sty;
// echo $this->json_encode($arr);
// exit;

        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['cat_id']) && $post_decode['cat_id'] != '') {


            $category_id = $post_decode['cat_id'];
            $category_id = $this->Category->getAllSubSubCategories($post_decode['cat_id']);
            $category_final = @implode(",", $category_id);
            $product = $this->Product->query(
                    "SELECT `Product`.`id`, `Product`.`frame_body_material`, `Product`.`upholstery_material`, `Product`.`fill_material`"
                    . ", `Product`.`metal_type`, `Category`.`id`, `ProductSpec`.`color`, `ProductSpec`.`dimensions`,ProductShipping.manner_of_shipping,"
                    . "Product.RugConstruction,Product.RugType"
                    . " FROM `eyestylist`.`products` AS `Product` "
                    . "LEFT JOIN `eyestylist`.`product_shippings` AS `ProductShipping` ON (`ProductShipping`.`product_id` = `Product`.`id`) "
                    . "LEFT JOIN `eyestylist`.`product_specs` AS `ProductSpec` ON (`ProductSpec`.`product_id` = `Product`.`id`) "
                    . "INNER JOIN `eyestylist`.`product_categories` AS `ProductCategory` ON (`ProductCategory`.`product_id`= `Product`.`id`) "
                    . "INNER JOIN `eyestylist`.`categories` AS `Category` ON (`Category`.`id` =`ProductCategory`.`category_id`) "
                    . " WHERE `Category`.`id` IN ({$category_final}) ");
        } else if (isset($post_decode['slider_id']) && $post_decode['slider_id'] != '') {
            $arrPWS = $this->ProductWhatSlider->find('first', array('conditions' => array('ProductWhatSlider.id' => $post_decode['slider_id']), 'recursive' => 1));
            if ($arrPWS['ProductWhatSlider']['choose_products'] == 0) {
                $post_decode['cat_id'] = $arrPWS['ProductWhatSlider']['category_id'];
                $category_id = $post_decode['cat_id'];
                $category_id = $this->Category->getAllSubSubCategories($post_decode['cat_id']);
                $category_final = @implode(",", $category_id);
                $product = $this->Product->query(
                        "SELECT `Product`.`id`, `Product`.`frame_body_material`, `Product`.`upholstery_material`, `Product`.`fill_material`"
                        . ", `Product`.`metal_type`, `Category`.`id`, `ProductSpec`.`color`, `ProductSpec`.`dimensions`,ProductShipping.manner_of_shipping,"
                        . "Product.RugConstruction,Product.RugType"
                        . " FROM `eyestylist`.`products` AS `Product` "
                        . "LEFT JOIN `eyestylist`.`product_shippings` AS `ProductShipping` ON (`ProductShipping`.`product_id` = `Product`.`id`) "
                        . "LEFT JOIN `eyestylist`.`product_specs` AS `ProductSpec` ON (`ProductSpec`.`product_id` = `Product`.`id`) "
                        . "INNER JOIN `eyestylist`.`product_categories` AS `ProductCategory` ON (`ProductCategory`.`product_id`= `Product`.`id`) "
                        . "INNER JOIN `eyestylist`.`categories` AS `Category` ON (`Category`.`id` =`ProductCategory`.`category_id`) "
                        . " WHERE `Category`.`id` IN ({$category_final}) ");
            } else {
                $prod = array();
                foreach ($arrPWS['ProductWhatSliderProduct'] as $pwsp) {
                    $prod[] = $pwsp['product_id'];
                }
                if (!empty($prod)) {
                    $product_final = @implode(",", $prod);
                } else {
                    $product_final = 0;
                }
                $product = $this->Product->query(
                        "SELECT `Product`.`id`, `Product`.`frame_body_material`, `Product`.`upholstery_material`, `Product`.`fill_material`"
                        . ", `Product`.`metal_type`, `Category`.`id`, `ProductSpec`.`color`, `ProductSpec`.`dimensions`,ProductShipping.manner_of_shipping,"
                        . "Product.RugConstruction,Product.RugType"
                        . " FROM `eyestylist`.`products` AS `Product` "
                        . "LEFT JOIN `eyestylist`.`product_shippings` AS `ProductShipping` ON (`ProductShipping`.`product_id` = `Product`.`id`) "
                        . "LEFT JOIN `eyestylist`.`product_specs` AS `ProductSpec` ON (`ProductSpec`.`product_id` = `Product`.`id`) "
                        . "INNER JOIN `eyestylist`.`product_categories` AS `ProductCategory` ON (`ProductCategory`.`product_id`= `Product`.`id`) "
                        . "INNER JOIN `eyestylist`.`categories` AS `Category` ON (`Category`.`id` =`ProductCategory`.`category_id`) "
                        . " WHERE `Product`.`id` IN ({$product_final}) ");
            }
        } else {
            $category_id = null;
            $category_final = '0';
            $product = $this->Product->query(
                    "SELECT `Product`.`id`, `Product`.`frame_body_material`, `Product`.`upholstery_material`, `Product`.`fill_material`"
                    . ", `Product`.`metal_type`, `Category`.`id`, `ProductSpec`.`color`, `ProductSpec`.`dimensions`,ProductShipping.manner_of_shipping,"
                    . "Product.RugConstruction,Product.RugType"
                    . " FROM `eyestylist`.`products` AS `Product` "
                    . "LEFT JOIN `eyestylist`.`product_shippings` AS `ProductShipping` ON (`ProductShipping`.`product_id` = `Product`.`id`) "
                    . "LEFT JOIN `eyestylist`.`product_specs` AS `ProductSpec` ON (`ProductSpec`.`product_id` = `Product`.`id`) "
                    . "INNER JOIN `eyestylist`.`product_categories` AS `ProductCategory` ON (`ProductCategory`.`product_id`= `Product`.`id`) "
                    . "INNER JOIN `eyestylist`.`categories` AS `Category` ON (`Category`.`id` =`ProductCategory`.`category_id`) "
                    . " WHERE `Category`.`id` IN ({$category_final}) ");
        }

        $allSubCat = $this->Category->getAllSubSubCategories(95);

        $frame_body_material = array();
        $upholstery_material = array();
        $fill_material = array();
        $metal_type = array();
        $color = array();
        $dimensions = array();
        $manner_of_shipping = array();
        $RugConstruction = array();
        $RugType = array();
        foreach ($product as $p) {
            $frame_body_material[] = trim($p['Product']['frame_body_material']);
            $upholstery_material[] = trim($p['Product']['upholstery_material']);
            $RugConstruction[] = trim($p['Product']['RugConstruction']);
            $RugType[] = trim($p['Product']['RugType']);
            $fill_material[] = trim($p['Product']['fill_material']);
            $metal_type[] = trim($p['Product']['metal_type']);
            $color[] = trim($p['ProductSpec']['color']);
            $dimensions[] = trim($p['ProductSpec']['dimensions']);
            $manner_of_shipping[] = trim($p['ProductShipping']['manner_of_shipping']);
        }

//        $frame_body_material = array_unique($frame_body_material);
//        $frame_body_material = array_filter($frame_body_material);
//        sort($frame_body_material);
//        if (!empty($frame_body_material)) {
//            $arr['Frame Body Material'] = $frame_body_material;
//        }
//95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,185,186
        array_push($allSubCat, @$post_decode['cat_id']);
     //   print_R($allSubCat);echo $post_decode['cat_id'];
        if (in_array(@$post_decode['cat_id'], $allSubCat)) {
            $RugConstruction = array_unique($RugConstruction);
            $RugConstruction = array_filter($RugConstruction);
            sort($RugConstruction);
            if (!empty($RugConstruction)) {
                $arr['Rug Construction'] = $RugConstruction;
            }

            $RugType = array_unique($RugType);
            $RugType = array_filter($RugType);
            sort($RugType);
            if (!empty($RugType)) {
                $arr['Rug Type'] = $RugType;
            }
        }
        $upholstery_material = array_unique($upholstery_material);
        $upholstery_material = array_filter($upholstery_material);
        sort($upholstery_material);
        if (!empty($upholstery_material)) {
            $arr['Upholestry Material'] = $upholstery_material;
        }
        $fill_material = array_unique($fill_material);
        $fill_material = array_filter($fill_material);
        sort($fill_material);
        if (!empty($fill_material)) {
            $arr['Fill Material'] = $fill_material;
        }

        $metal_type = array_unique($metal_type);
        $metal_type = array_filter($metal_type);
        sort($metal_type);
        if (!empty($metal_type)) {
            $arr['Metal Type'] = $metal_type;
        }

        $color = array_unique($color);
        $color = array_filter($color);
        sort($color);
        if (!empty($color)) {
            $arr['Color'] = $color;
        }
        $dimensions = array_unique($dimensions);
        $dimensions = array_filter($dimensions);
        sort($dimensions);
        if (!empty($dimensions)) {
            $arr['Dimensions'] = $dimensions;
        }
//        $manner_of_shipping = array_unique($manner_of_shipping);
//        $manner_of_shipping = array_filter($manner_of_shipping);
//        sort($manner_of_shipping);
//        if (!empty($manner_of_shipping)) {
//            $arr['Manner of shipping'] = $manner_of_shipping;
//        }

        echo $this->json_encode($arr);
        exit;


        print_R($arr);
    }

    function getDimensions() {
        $post_decode = $this->request->input('json_decode', true);
        if (!empty($post_decode['prod_id'])) {
            $diamention = $this->ProductSpec->find('first', array(
                'conditions' => array('ProductSpec.product_id' => $post_decode['prod_id']),
                'fields' => array('ProductSpec.dimensions', 'ProductSpec.product_id'),
                'recursive' => -1));
            $d['prod_id'] = $diamention['ProductSpec']['product_id'];
            $d['dimensions'] = $diamention['ProductSpec']['dimensions'];
            echo $this->json_encode(array('status' => 'success', 'data' => $d));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => 'Please enter prouct_id'));
        }
    }

    function createOrder() {

        $post_decode = $this->request->input('json_decode', true);

        if (isset($post_decode['other_user_id']) && $post_decode['other_user_id'] != '') {
            $this->CartItem->virtualFields = array('subtotal' => 'CartItem.quantity * Product.price');
            $allCart = $this->CartItem->find('all', array('recursive' => 0, 'conditions' => array('CartItem.other_user_id' => $post_decode['other_user_id']),
                'fields' => array('CartItem.id', 'CartItem.created', 'Product.id', 'Product.prod_name', 'Product.price', 'CartItem.quantity', 'subtotal')));

            if ($allCart) {

                $arr = array();
                $total = '';
                foreach ($allCart as $k => $cart) {
                    $total = $total + $cart['CartItem']['subtotal'];
                }
                //card_token
                //payment_id,status
                $total = number_format($total + 2.95, 2, '.', '');
                $arr2 = array();
                $arr2['Order']['other_user_id'] = $post_decode['other_user_id'];
                $arr2['Order']['other_user_shipping_add_id'] = $post_decode['other_user_shipping_add_id'];
                // $arr2['Order']['payment_id'] = $post_decode['payment_id'];
                // $arr2['Order']['status'] = $post_decode['status'];
                $arr2['Order']['total'] = $total;
                $arr2['Order']['order_uniqueid'] = uniqid('ORD');

                /*                 * ******Stripe************** */
                $user = $this->OtherUser->find('first', array('conditions' => array('OtherUser.id' => $post_decode['other_user_id'])));
                if (isset($user['OtherUser']['stripe_customer']) && $user['OtherUser']['stripe_customer'] == '') {

                    $data = array(
                        // 'stripeToken' =>  $Token['stripe_id'],
                        'description' => $user['OtherUser']['firstName'] . ' ' . $user['OtherUser']['lastName'],
                        'email' => $user['OtherUser']['email']
                    );
                    $result = $this->Stripe->customerCreate($data);
                    $strip_customer = $result['stripe_id'];
                    $arrOU['OtherUser']['id'] = $post_decode['other_user_id'];
                    $arrOU['OtherUser']['stripe_customer'] = $strip_customer;
                    $this->OtherUser->save($arrOU);
                } else {
                    $strip_customer = $user['OtherUser']['stripe_customer'];
                }

                $result2 = $this->Stripe->customerRetrieve($strip_customer);
                if ($result2->deleted) {
                    $data = array(
                        // 'stripeToken' =>  $Token['stripe_id'],
                        'description' => $user['OtherUser']['firstName'] . ' ' . $user['OtherUser']['lastName'],
                        'email' => $user['OtherUser']['email']
                    );
                    $result = $this->Stripe->customerCreate($data);
                    $strip_customer = $result['stripe_id'];
                    $arrOU['OtherUser']['id'] = $post_decode['other_user_id'];
                    $arrOU['OtherUser']['stripe_customer'] = $strip_customer;
                    $this->OtherUser->save($arrOU);
                }

                //  $Token = $this->Stripe->createToken(''); 
                // $card_token=$Token['stripe_id'];
                $card_token = $post_decode['cart_token'];
                $cu = $this->Stripe->updateCard($strip_customer, $card_token);

                $result3 = $this->Stripe->customerRetrieve($strip_customer);

                $chargeData['amount'] = $arr2['Order']['total'] * 100;
                $chargeData['currency'] = 'usd';
                $chargeData['customer'] = $strip_customer;
                //$chargeData['card'] = $Token['stripe_id'];
                $charge = $this->Stripe->createOrder($chargeData);

                $arr2['Order']['stripe_charge'] = $charge['stripe_id'];
                $arr2['Order']['stripe_transaction'] = $charge['txn_id'];
                $arr2['Order']['stripe_customer'] = $charge['customer'];
                $arr2['Order']['stripe_payment_status'] = $charge['paid'];

                /*                 * ******Stripe************** */




                if ($order = $this->Order->save($arr2)) {
                    foreach ($allCart as $k => $cart) {
                        $arr[$k]['OrderDetail']['product_id'] = $cart['Product']['id'];
                        $arr[$k]['OrderDetail']['quantity'] = $cart['CartItem']['quantity'];
                        $arr[$k]['OrderDetail']['price'] = $cart['Product']['price'];
                        $arr[$k]['OrderDetail']['order_id'] = $order['Order']['id'];
                    }

                    $this->OrderDetail->saveAll($arr);
                    echo $this->json_encode(array('status' => 'success', 'order_id' => $order['Order']['id'], 'msg' => 'payment success'));
                    $this->CartItem->deleteAll(array('other_user_id' => $post_decode['other_user_id']));
                } else {
                    echo $this->json_encode(array('status' => 'fail', 'msg' => ''));
                }
            } else {
                echo $this->json_encode(array('status' => 'fail', 'msg' => 'cart is empty'));
            }

            /* {"other_user_id":5,"payment_id":"1","status":1,"other_user_shipping_add_id":1} */
        }
    }

    function listOrders() {
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['other_user_id']) && $post_decode['other_user_id'] != '') {
            $orders = $this->Order->find('all', array('conditions' => array('Order.other_user_id' => $post_decode['other_user_id']), 'recursive' => 2, 'order' => 'Order.id desc'));

            $arr = array();
            foreach ($orders as $k => $od) {
                $arr[$k]['order_id'] = $od['Order']['id'];
                $arr[$k]['order_uniqueid'] = $od['Order']['order_uniqueid'];
                $arr[$k]['total'] = $od['Order']['total'];
                foreach ($od['OrderDetail'] as $k1 => $ods) {
                    // print_R($ods['Product']['prod_name']);
                    $arr[$k]['products'][$k1]['prod_name'] = $ods['Product']['prod_name'];
                    $arr[$k]['products'][$k1]['quantity'] = $ods['quantity'];
                }
            }
            //  print_R($arr);
            //  exit;

            if ($arr) {
                echo $this->json_encode(array('status' => 'success', 'orders' => $arr));
            } else {
                echo $this->json_encode(array('status' => 'fail', 'msg' => 'no orders'));
            }
        }
        /* {"other_user_id":5} */
    }

    function viewOrder() {
        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['other_user_id']) && $post_decode['other_user_id'] != '') {
            $o = $this->Order->find('first', array('conditions' => array('Order.other_user_id' => $post_decode['other_user_id'], 'Order.id' => $post_decode['order_id']), 'recursive' => 1));
            $order = array();

            /* Order */
            $order['order']['id'] = $o['Order']['id'];
            $order['order']['order_uniqueid'] = $o['Order']['order_uniqueid'];
            $order['order']['other_user_id'] = $o['Order']['other_user_id'];
            $order['order']['other_user_shipping_add_id'] = $o['Order']['other_user_shipping_add_id'];
            // $order['order']['payment_id'] = $o['Order']['payment_id'];
            $order['order']['order_date'] = $o['Order']['date'];
            $order['order']['status'] = $o['Order']['status'];
            /* Order Details */
            foreach ($o['OrderDetail'] as $k => $od) {

                $sql = "select prod_name,color,price,price*{$od['quantity']} as total,"
                        . "(
    CASE 
        WHEN product_specs.dimensions = '' THEN products.construction        
        ELSE product_specs.dimensions
    END) AS dimensions2 ,product_images.prod_img   from products left join product_specs on products.id=product_specs.product_id 
    left join product_images on products.id=product_images.product_id
    where products.id={$od['product_id']} limit 0,1 ";
                $p = $this->Product->query($sql);
                // print_R($p);
                $order['order_detail'][$k]['id'] = $od['id'];
                $order['order_detail'][$k]['product_id'] = $od['product_id'];
                $order['order_detail'][$k]['quantity'] = $od['quantity'];
                $order['order_detail'][$k]['price'] = $od['price'];
                $order['order_detail'][$k]['total'] = $p[0][0]['total'];
                $order['order_detail'][$k]['product_name'] = $p[0]['products']['prod_name'];
                $order['order_detail'][$k]['color'] = $p[0]['product_specs']['color'];
                $order['order_detail'][$k]['dimensions'] = $p[0][0]['dimensions2'];
                $order['order_detail'][$k]['product_images'] = $p[0]['product_images']['prod_img'];
            }


            $order['shipping_info'] = $o['OtherUserShippingAdd'];
            //$order['shipping_info']['phone'] = "";
            $order['payment']['mode_of_payment'] = 'Credit Card';

            if (!empty($order)) {
                echo $this->json_encode(array('status' => 'success', 'orders' => $order));
            } else {
                echo $this->json_encode(array('status' => 'fail', 'msg' => 'no orders'));
            }
        }
        /* {"other_user_id":5,"order_id":1} */
    }

    function UpdateMyAccount() {
        $post_decode = $this->request->input('json_decode', true);
        if (!empty($post_decode['other_user_id'])) {
            if ($post_decode['firstname'] != '') {
                $arr['OtherUser']['firstName'] = $post_decode['firstname'];
            }
            $arr['OtherUser']['lastName'] = $post_decode['lastname'];
            $sql = $this->OtherUser->find('first', array('conditions' => array('email' => $post_decode['email'], 'id !=' => $post_decode['other_user_id'])));
            if ($sql) {
                echo $this->json_encode(array('status' => 'fail', 'msg' => 'Email already exist!'));
                exit;
            }
            $arr['OtherUser']['email'] = $post_decode['email'];
            if ($post_decode['password'] != '') {
                $arr['OtherUser']['password'] = md5($post_decode['password']);
            }
            $arr['OtherUser']['phone'] = $post_decode['phone'];
            $arr['OtherUser']['id'] = $post_decode['other_user_id'];

            $d = $this->OtherUser->save($arr);
            echo $this->json_encode(array('status' => 'success', 'data' => $d));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => 'Somthing wrong'));
        }
    }

    /* Get All Dimensions for camera screen */

    function getAllDimensions() {
        $post_decode = $this->request->input('json_decode', true);
        if (!empty($post_decode['prod_id'])) {
            $sql = "SELECT Product.id, ProductSpec.dimensions,Product.`construction` FROM products AS Product"
                    . " LEFT JOIN `eyestylist`.`product_specs` AS `ProductSpec` ON (`ProductSpec`.`product_id` = `Product`.`id`)  "
                    . "inner JOIN (SELECT * FROM `product_images` WHERE (`mainImage`=1 OR addinroom=1) ORDER BY `mainImage` DESC ,`addinroom` DESC) AS ProductImage ON ProductImage.product_id=Product.id  "
                    . " WHERE parent_style_number = (SELECT parent_style_number FROM products WHERE  id ='{$post_decode['prod_id']}') and parent_style_number<> '' "; //a

            $filters = $this->Product->query($sql);
            $arrFilterConstruction2 = $arrFilterD2 = $arrFilterConstruction = $arrFilterD = array();
            foreach ($filters AS $f) {
                if ($f['ProductSpec']['dimensions'] != '') {
                    $arrFilterD[$f['Product']['id']] = $f['ProductSpec']['dimensions'];
                }
                $arrFilterConstruction[$f['Product']['id']] = $f['Product']['construction'];
            }
            $arrFilterD = array_unique($arrFilterD);
            $arrFilterD = array();
            if (sizeof($arrFilterD) <= 0) {
                $arrFilterD = array();
                $arrFilterD = array_unique($arrFilterConstruction);
                if (sizeof($arrFilterD) <= 0) {
                    $arrFilterD = array();
                }
            }
            echo $this->json_encode(array('status' => 'success', 'data' => array('Dimensions' => $arrFilterD)));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => 'Please enter prouct_id'));
        }
    }

    function forgotPassword() {
        Configure::write('debug', 0);

        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['email']) && $post_decode['email'] != '') {
            $check = $this->OtherUser->find('first', array('conditions' => array('OtherUser.email' => $post_decode['email'])));
            if ($check) {

                $time = time();
                $array['OtherUser']['id'] = $check['OtherUser']['id'];
                $array['OtherUser']['forgot_hash'] = md5($time);
                $array['OtherUser']['forgot_expire'] = date('Y-m-d H:i:s', $time + 86400);
                if ($this->OtherUser->save($array)) {
                    $link1 = Router::url(array('controller' => 'recovers', 'action' => 'reset', 'hash' => $array['OtherUser']['forgot_hash']), true);
                    $Email = new CakeEmail();
                    $Email->config('default');
                    $Email->subject('[EYELY]Please reset your password');
                    $Email->viewVars(array('link1' => $link1));
                    $Email->template('forgot', 'default')
                            ->emailFormat('html')
                            ->to($post_decode['email'])
                            ->from(array('admin@eyely.com' => 'EYELY'))
                            ->send();
                    echo $this->json_encode(array('status' => 'success', 'msg' => 'Reset password link has been sent your email id please check.'));
                } else {
                    echo $this->json_encode(array('status' => 'fail', 'msg' => 'Something wrong.'));
                }
            } else {
                echo $this->json_encode(array('status' => 'fail', 'msg' => 'Email ID is not in our database.'));
            }
        } else {
            echo $this->json_encode(array('status' => 'fail', 'msg' => 'Please provide Email ID'));
        }
    }

    function getProductsForSliderImages() {
// Configure::write('debug',2);
//{"cat_id":"1","offset":0 ,"style_id":1,"filters":{"Price":0,"Styles":1,"Manner of shipping":"FEDEX/UPS","Metal Type":"ZINC ALLOY","Color":"BROWN","Dimensions":"17\" x 17\" x 23\""}}

        $post_decode = $this->request->input('json_decode', true);
        if (isset($post_decode['slider_id']) && $post_decode['slider_id'] != '') {
            $arrPWS = $this->ProductWhatSlider->find('first', array('conditions' => array('ProductWhatSlider.id' => $post_decode['slider_id']), 'recursive' => 1));

            if ($arrPWS['ProductWhatSlider']['choose_products'] == 0) {
                $post_decode['cat_id'] = $arrPWS['ProductWhatSlider']['category_id'];
                if (isset($post_decode['cat_id']) && $post_decode['cat_id'] != '') {
                    $category_id = $post_decode['cat_id'];
                    $category_id = $this->Category->getAllSubSubCategories($post_decode['cat_id']);
                } else {
                    $category_id = null;
                }
                if (isset($post_decode['other_user_id']) && $post_decode['other_user_id'] != '') {
                    $other_user_id = $post_decode['other_user_id'];
                    $styles = $this->UserStyle->find('all', array('conditions' => array('UserStyle.other_user_id' => $other_user_id), 'order' => 'UserStyle.primary_style desc'));

                    if ($styles) {
                        $order_style = "";
                        foreach ($styles as $style) {
                            $order_style .= " style{$style['UserStyle']['style_id']}_order desc, ";
                        }
                    } else {
                        $order_style = "";
                    }
                } else {
                    $other_user_id = 0;
                    $order_style = "";
                }
                $where = "";
                $order = "";

                if (isset($post_decode['filters']) && $post_decode['filters'] != '') {
                    if (isset($post_decode['filters']['Styles']) && $post_decode['filters']['Styles'] != '') {
                        $where .= " AND style{$post_decode['filters']['Styles']}='1'";
                    }
                    if (isset($post_decode['filters']['Manner of shipping']) && $post_decode['filters']['Manner of shipping'] != '') {
                        $post_decode['filters']['Manner of shipping'] = addslashes($post_decode['filters']['Manner of shipping']);
                        $where .= " AND ProductShipping.manner_of_shipping='{$post_decode['filters']['Manner of shipping']}'";
                    }
                    if (isset($post_decode['filters']['Metal Type']) && $post_decode['filters']['Metal Type'] != '') {
                        $post_decode['filters']['Metal Type'] = addslashes($post_decode['filters']['Metal Type']);
                        $where .= " AND Product.metal_type='{$post_decode['filters']['Metal Type']}'";
                    }
                    if (isset($post_decode['filters']['Fill Material']) && $post_decode['filters']['Fill Material'] != '') {
                        $post_decode['filters']['Fill Material'] = addslashes($post_decode['filters']['Fill Material']);
                        $where .= " AND Product.fill_material='{$post_decode['filters']['Fill Material']}'";
                    }
                    if (isset($post_decode['filters']['Frame Body Material']) && $post_decode['filters']['Frame Body Material'] != '') {
                        $post_decode['filters']['Frame Body Material'] = addslashes($post_decode['filters']['Frame Body Material']);
                        $where .= " AND Product.frame_body_material='{$post_decode['filters']['Frame Body Material']}'";
                    }
                    if (isset($post_decode['filters']['Upholestry Material']) && $post_decode['filters']['Upholestry Material'] != '') {
                        $post_decode['filters']['Upholestry Material'] = addslashes($post_decode['filters']['Upholestry Material']);
                        $where .= " AND Product.upholstery_material='{$post_decode['filters']['Upholestry Material']}'";
                    }
                    if (isset($post_decode['filters']['Rug Construction']) && $post_decode['filters']['Rug Construction'] != '') {
                        $post_decode['filters']['Rug Construction'] = addslashes($post_decode['filters']['Rug Construction']);
                        $where .= " AND Product.RugConstruction='{$post_decode['filters']['Rug Construction']}'";
                    }
                    if (isset($post_decode['filters']['Rug Type']) && $post_decode['filters']['Rug Type'] != '') {
                        $post_decode['filters']['Rug Type'] = addslashes($post_decode['filters']['Rug Type']);
                        $where .= " AND Product.RugType='{$post_decode['filters']['Rug Type']}'";
                    }
                    if (isset($post_decode['filters']['Color']) && $post_decode['filters']['Color'] != '') {
                        $post_decode['filters']['Color'] = addslashes($post_decode['filters']['Color']);
                        $where .= " AND ProductSpec.color='{$post_decode['filters']['Color']}'";
                    }
                    if (isset($post_decode['filters']['Dimensions']) && $post_decode['filters']['Dimensions'] != '') {
                        $post_decode['filters']['Dimensions'] = addslashes($post_decode['filters']['Dimensions']);
                        $where .= " AND ProductSpec.dimensions='{$post_decode['filters']['Dimensions']}'";
                    }

                    if (isset($post_decode['filters']['Price']) && $post_decode['filters']['Price'] !== '') {
                        if ($post_decode['filters']['Price'] == 0) {
                            $order = "order by price asc ,  $order_style category_order desc";
                        } else {
                            $order = "order by price desc ,  $order_style category_order desc";
                        }
                    } else {
                        $order = "order by  $order_style category_order desc";
                    }
                } else {
                    if (isset($post_decode['order']) && $post_decode['order'] != '') {
                        $order = "order by " . $post_decode['order'] . " , {$order_style} category_order desc";
                    } else {
                        $order = "order by  $order_style category_order desc";
                    }

                    if (isset($post_decode['style_id']) && $post_decode['style_id'] != '') {
                        $where = " AND style{$post_decode['style_id']}='1'";
                    }
                }
                //   echo $order;exit;

                $category_final = implode(",", $category_id);

                if (isset($post_decode['page']) && $post_decode['page'] != '') {
                    $page = $post_decode['page'];
                } else {
                    $page = 1;
                }
                $limit = 100;
                if (isset($post_decode['offset']) && $post_decode['offset'] != '') {
                    $offset = $post_decode['offset'];
                } else {
                    $offset = 0;
                }

                $product = $this->Product->query(
                        "SELECT `id`,null as more_products,`prod_name`,parent_style_number,`price`,`available_units`,`status`, prod_img,prod_thumb,category_order,style2_order,style3_order,style4_order,style5_order FROM ("
                        . "SELECT `Product`.`id`, `Product`.`prod_name`, `Product`.`price`,Product.parent_style_number , `Product`.`available_units`,  `UserFavorite`.`status`,"
                        . "ProductImage.prod_img,ProductImage.prod_thumb,category_order,style1_order,style2_order,style3_order,style4_order,style5_order FROM `eyestylist`.`products` AS `Product` "
                        . "LEFT JOIN `eyestylist`.`product_shippings` AS `ProductShipping` ON (`ProductShipping`.`product_id` = `Product`.`id`) "
                        . "LEFT JOIN `eyestylist`.`product_specs` AS `ProductSpec` ON (`ProductSpec`.`product_id` = `Product`.`id`) "
                        . "LEFT JOIN `eyestylist`.`user_favorites` AS `UserFavorite` ON (`UserFavorite`.`other_user_id`=0 AND `UserFavorite`.`product_id` = `Product`.`id`) "
                        . "INNER JOIN `eyestylist`.`product_categories` AS `ProductCategory` ON (`ProductCategory`.`product_id`= `Product`.`id`) "
                        . "INNER JOIN `eyestylist`.`categories` AS `Category` ON (`Category`.`id` =`ProductCategory`.`category_id`) "
                        . "inner JOIN (SELECT * FROM `product_images` WHERE (`mainImage`=1 OR addinroom=1) ORDER BY `mainImage` DESC ,`addinroom` DESC) AS ProductImage ON ProductImage.product_id=Product.id  "
                        . " WHERE `Category`.`id` IN ({$category_final}) $where GROUP BY ProductImage.`product_id` $order ) AS Product   "
                        . "  $order LIMIT {$limit} OFFSET {$offset} ");

                $arr = array();
                foreach ($product as $k => $p) {
                    if ($product[$k]['Product']['price'] == null) {
                        $product[$k]['Product']['price'] = '0.00';
                    }
                    $arr[$k]['id'] = $product[$k]['Product']['id'];
                    $arr[$k]['prod_name'] = $product[$k]['Product']['prod_name'];
                    $arr[$k]['price'] = $product[$k]['Product']['price'];
                    $arr[$k]['available_units'] = $product[$k]['Product']['available_units'];
                    $arr[$k]['more_products'] = $product[$k][0]['more_products'];
                    if (isset($product[$k]['Product']['prod_img'])) {
                        $arr[$k]['prod_img'] = $product[$k]['Product']['prod_img'];
                        $arr[$k]['prod_thumb'] = $product[$k]['Product']['prod_thumb'];

                        if ($product[$k]['Product']['status'] == null) {
                            $arr[$k]['favorite'] = 0;
                        } else {
                            $arr[$k]['favorite'] = 1;
                        }
                        unset($product[$k]['ProductImage']);
                        unset($product[$k]['Product']);
                    }
                }
            } else {
                $prod = array();
                foreach ($arrPWS['ProductWhatSliderProduct'] as $pwsp) {
                    $prod[] = $pwsp['product_id'];
                }
                if (isset($post_decode['other_user_id']) && $post_decode['other_user_id'] != '') {
                    $other_user_id = $post_decode['other_user_id'];
                    $styles = $this->UserStyle->find('all', array('conditions' => array('UserStyle.other_user_id' => $other_user_id), 'order' => 'UserStyle.primary_style desc'));

                    if ($styles) {
                        $order_style = "";
                        foreach ($styles as $style) {
                            $order_style .= " style{$style['UserStyle']['style_id']}_order desc, ";
                        }
                    } else {
                        $order_style = "";
                    }
                } else {
                    $other_user_id = 0;
                    $order_style = "";
                    ;
                }
                $where = "";
                $order = "";

                if (isset($post_decode['filters']) && $post_decode['filters'] != '') {
                    if (isset($post_decode['filters']['Styles']) && $post_decode['filters']['Styles'] != '') {
                        $where .= " AND style{$post_decode['filters']['Styles']}='1'";
                    }
                    if (isset($post_decode['filters']['Manner of shipping']) && $post_decode['filters']['Manner of shipping'] != '') {
                        $post_decode['filters']['Manner of shipping'] = addslashes($post_decode['filters']['Manner of shipping']);
                        $where .= " AND ProductShipping.manner_of_shipping='{$post_decode['filters']['Manner of shipping']}'";
                    }
                    if (isset($post_decode['filters']['Metal Type']) && $post_decode['filters']['Metal Type'] != '') {
                        $post_decode['filters']['Metal Type'] = addslashes($post_decode['filters']['Metal Type']);
                        $where .= " AND Product.metal_type='{$post_decode['filters']['Metal Type']}'";
                    }
                    if (isset($post_decode['filters']['Fill Material']) && $post_decode['filters']['Fill Material'] != '') {
                        $post_decode['filters']['Fill Material'] = addslashes($post_decode['filters']['Fill Material']);
                        $where .= " AND Product.fill_material='{$post_decode['filters']['Fill Material']}'";
                    }
                    if (isset($post_decode['filters']['Frame Body Material']) && $post_decode['filters']['Frame Body Material'] != '') {
                        $post_decode['filters']['Frame Body Material'] = addslashes($post_decode['filters']['Frame Body Material']);
                        $where .= " AND Product.frame_body_material='{$post_decode['filters']['Frame Body Material']}'";
                    }
                    if (isset($post_decode['filters']['Upholestry Material']) && $post_decode['filters']['Upholestry Material'] != '') {
                        $post_decode['filters']['Upholestry Material'] = addslashes($post_decode['filters']['Upholestry Material']);
                        $where .= " AND Product.upholstery_material='{$post_decode['filters']['Upholestry Material']}'";
                    }
                    if (isset($post_decode['filters']['Rug Construction']) && $post_decode['filters']['Rug Construction'] != '') {
                        $post_decode['filters']['Rug Construction'] = addslashes($post_decode['filters']['Rug Construction']);
                        $where .= " AND Product.RugConstruction='{$post_decode['filters']['Rug Construction']}'";
                    }
                    if (isset($post_decode['filters']['Rug Type']) && $post_decode['filters']['Rug Type'] != '') {
                        $post_decode['filters']['Rug Type'] = addslashes($post_decode['filters']['Rug Type']);
                        $where .= " AND Product.RugType='{$post_decode['filters']['Rug Type']}'";
                    }
                    if (isset($post_decode['filters']['Color']) && $post_decode['filters']['Color'] != '') {
                        $post_decode['filters']['Color'] = addslashes($post_decode['filters']['Color']);
                        $where .= " AND ProductSpec.color='{$post_decode['filters']['Color']}'";
                    }
                    if (isset($post_decode['filters']['Dimensions']) && $post_decode['filters']['Dimensions'] != '') {
                        $post_decode['filters']['Dimensions'] = addslashes($post_decode['filters']['Dimensions']);
                        $where .= " AND ProductSpec.dimensions='{$post_decode['filters']['Dimensions']}'";
                    }

                    if (isset($post_decode['filters']['Price']) && $post_decode['filters']['Price'] !== '') {
                        if ($post_decode['filters']['Price'] == 0) {
                            $order = "order by price asc ,  $order_style category_order desc";
                        } else {
                            $order = "order by price desc ,  $order_style category_order desc";
                        }
                    } else {
                        $order = "order by  $order_style category_order desc";
                    }
                } else {
                    if (isset($post_decode['order']) && $post_decode['order'] != '') {
                        $order = "order by " . $post_decode['order'] . " , {$order_style} category_order desc";
                    } else {
                        $order = "order by  $order_style category_order desc";
                    }

                    if (isset($post_decode['style_id']) && $post_decode['style_id'] != '') {
                        $where = " AND style{$post_decode['style_id']}='1'";
                    }
                }

                $products = implode(",", $prod);

                if (isset($post_decode['page']) && $post_decode['page'] != '') {
                    $page = $post_decode['page'];
                } else {
                    $page = 1;
                }
                $limit = 100;
                if (isset($post_decode['offset']) && $post_decode['offset'] != '') {
                    $offset = $post_decode['offset'];
                } else {
                    $offset = 0;
                }
                /* With more product */

                $product = $this->Product->query(
                        "SELECT `id`,null as more_products,`prod_name`,parent_style_number,`price`,`available_units`,`status`, prod_img,prod_thumb,category_order,style2_order,style3_order,style4_order,style5_order FROM ("
                        . "SELECT `Product`.`id`, `Product`.`prod_name`, `Product`.`price`,Product.parent_style_number , `Product`.`available_units`,  `UserFavorite`.`status`,"
                        . "ProductImage.prod_img,ProductImage.prod_thumb,category_order,style1_order,style2_order,style3_order,style4_order,style5_order FROM `eyestylist`.`products` AS `Product` "
                        . "LEFT JOIN `eyestylist`.`product_shippings` AS `ProductShipping` ON (`ProductShipping`.`product_id` = `Product`.`id`) "
                        . "LEFT JOIN `eyestylist`.`product_specs` AS `ProductSpec` ON (`ProductSpec`.`product_id` = `Product`.`id`) "
                        . "LEFT JOIN `eyestylist`.`user_favorites` AS `UserFavorite` ON (`UserFavorite`.`other_user_id`=0 AND `UserFavorite`.`product_id` = `Product`.`id`) "
                        . "INNER JOIN `eyestylist`.`product_categories` AS `ProductCategory` ON (`ProductCategory`.`product_id`= `Product`.`id`) "
                        . "INNER JOIN `eyestylist`.`categories` AS `Category` ON (`Category`.`id` =`ProductCategory`.`category_id`) "
                        . "inner JOIN (SELECT * FROM `product_images` WHERE (`mainImage`=1 OR addinroom=1) ORDER BY `mainImage` DESC ,`addinroom` DESC) AS ProductImage ON ProductImage.product_id=Product.id  "
                        . " WHERE `Product`.`id` IN ({$products}) $where GROUP BY ProductImage.`product_id` $order ) AS Product   "
                        . "  $order LIMIT {$limit} OFFSET {$offset} ");

                $arr = array();
                foreach ($product as $k => $p) {
                    if ($product[$k]['Product']['price'] == null) {
                        $product[$k]['Product']['price'] = '0.00';
                    }
                    $arr[$k]['id'] = $product[$k]['Product']['id'];
                    $arr[$k]['prod_name'] = $product[$k]['Product']['prod_name'];
                    $arr[$k]['price'] = $product[$k]['Product']['price'];
                    $arr[$k]['available_units'] = $product[$k]['Product']['available_units'];
                    $arr[$k]['more_products'] = $product[$k][0]['more_products'];
                    if (isset($product[$k]['Product']['prod_img'])) {
                        $arr[$k]['prod_img'] = $product[$k]['Product']['prod_img'];
                        $arr[$k]['prod_thumb'] = $product[$k]['Product']['prod_thumb'];

                        if ($product[$k]['Product']['status'] == null) {
                            $arr[$k]['favorite'] = 0;
                        } else {
                            $arr[$k]['favorite'] = 1;
                        }
                        unset($product[$k]['ProductImage']);
                        unset($product[$k]['Product']);
                    }
                }
            }
        }

        if (!empty($arr)) {
            echo $this->json_encode(array('status' => 'success', 'data' => $arr, 'offset' => $offset + $limit));
        } else {
            echo $this->json_encode(array('status' => 'fail', 'data' => 'No products found.'));
        }
    }

}
