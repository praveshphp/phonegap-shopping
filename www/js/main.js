function check() {

    var un = document.getElementById('username').value;
    var action = 'check';
    $.getJSON(domain + '/json_backend.php?callback=?', 'un=' + un + '&action=' + action, function (res) {

        //document.write('<b>'+res.status+'</b><br>');
        document.getElementById('result').innerHTML = '<b>' + res.status + '</b>';

    });

}

function login() {

    var un = document.getElementById("username").value;
    var pw = document.getElementById("password").value;

    //The action
    var action = 'signin';
    $.ajax({
        type: "POST",
        url: domain + '/' + action,
        data: JSON.stringify({email: un, password: pw}),
        dataType: 'json',
        contentType: 'application/json; charset=UTF-8',
        context: this,
        success: function (res)
        {
            console.info(res);
            if (res.status == "success") {
                document.addEventListener("deviceready", onDeviceReady, false);
                window.localStorage.setItem("username", un);
                window.localStorage.setItem("password", pw);
                window.localStorage.setItem("uid", res.data.id);
                window.location = "main.html";
            } else {
                document.getElementById('message').innerHTML = '<b>' + res.status + '</b>';
            }
        }
    });

}

function whatsnew() {
    var uid = window.localStorage.getItem("uid");
    var action = 'whatsnew';
    $.ajax({
        type: "POST",
        url: domain + '/' + action,
        data: JSON.stringify({other_user_id: uid}),
        dataType: 'json',
        contentType: 'application/json; charset=UTF-8',
        context: this,
        success: function (res)
        {

            if (res.status == "success") {

                $.each(res.data, function (i, item) {

                    $('ul#output').append('<li><a href="./category.html" onclick="getProductsByCategoryRedirect(' + item.ProductWhat.Category_id + ',' + item.ProductWhat.style_id + ',' + item.ProductWhat.direct + ',' + item.ProductWhat.show_categories + ',\'' + item.ProductWhat.display_name + '\')"><img width="160" height="160" src="' + item.ProductWhat.Product_img + '"><h2>' + item.ProductWhat.display_name + '</h2><p>' + item.ProductWhat.display_name + '</p></a><!--<a href="#purchase" data-rel="popup" data-position-to="window" data-transition="pop">Purchase album</a>--></li>');

                });
                $("ul#output").listview("refresh");
            } else {
                document.getElementById('message').innerHTML = '<b>' + res.status + '</b>';
            }
        }
    });
}

function getProductsByCategoryRedirect(cat_id, style_id, direct, show_categories,display_name) {
    window.localStorage.setItem('cat_id', cat_id);
    window.localStorage.setItem('style_id', style_id);
    window.localStorage.setItem('direct', direct);
    window.localStorage.setItem('show_categories', show_categories);
    window.localStorage.setItem('display_name', display_name);
   // window.location = "category.html";
   $.mobile.changePage( "./category.html", { transition: "slideup", changeHash: false });
}
function getProductsByCategory() {
    var other_user_id = window.localStorage.getItem("uid");
    var cat_id = window.localStorage.getItem("cat_id");
    var style_id = window.localStorage.getItem("style_id");
    var direct = window.localStorage.getItem("direct");
    var show_categories = window.localStorage.getItem("show_categories");
    var display_name = window.localStorage.getItem("display_name");
    $('#title').text(display_name);
    var action = 'getProductsByCategory';
    $.ajax({
        type: "POST",
        url: domain + '/' + action,
        data: JSON.stringify({other_user_id: other_user_id, cat_id: cat_id, style_id: style_id}),
        dataType: 'json',
        contentType: 'application/json; charset=UTF-8',
        context: this,
        success: function (res)
        {

            if (res.status == "success") {
                var html='';
                $.each(res.data, function (i, item) {
                    console.info(item);
                    if(i%2==0 || i==0){
                        html +='<div class = "ui-grid-a">';
                    }
                    if(i%2==0){
                    html += '<div class = "ui-block-a"> <div class = "ui-bar ui-bar-e" style = "height:120px">\n\
'+'<div class="ui-block-a product-thumb"><img src="'+item.prod_thumb+'" class="thumb-img"></div>'+'\
'+'<div class="ui-block-a product-title">'+item.prod_name+'</div>'+'\
'+'<div class="ui-block-a product-price">'+item.price+'</div>'+'\
</div></div>';
                    }else{
                        html += '<div class = "ui-block-b"> <div class = "ui-bar ui-bar-e" style = "height:120px">\n\
'+'<div class="ui-block-a product-thumb"><img src="'+item.prod_thumb+'" class="thumb-img"></div>'+'\
'+'<div class="ui-block-a product-title">'+item.prod_name+'</div>'+'\
'+'<div class="ui-block-a product-price">'+item.price+'</div>'+'\
</div></div>';
                    }
                    if(i%2==1 && i!=0){
                        html +='</div>';
                    }
                    $('#Products').html(html);
                            //$('ul#output').append('<li><a href="javascript:void(0)" onclick="getProductsByCategoryRedirect('+item.ProductWhat.Category_id+','+item.ProductWhat.style_id+','+item.ProductWhat.direct+','+item.ProductWhat.show_categories+')"><img width="160" height="160" src="' + item.ProductWhat.Product_img +  '"><h2>' + item.ProductWhat.display_name + '</h2><p>' + item.ProductWhat.display_name + '</p></a><!--<a href="#purchase" data-rel="popup" data-position-to="window" data-transition="pop">Purchase album</a>--></li>');                  
                });
                $("ul#output").listview("refresh");
            } else {
                document.getElementById('message').innerHTML = '<b>' + res.status + '</b>';
            }
        }
    });
}
function forgot() {

    var em = document.getElementById("email").value;

    //The action
    var action = 'forgot';

    $.getJSON(domain + '/json_backend.php?callback=?', 'em=' + em + '&action=' + action, function (res) {

        document.write('<b>' + res.status + '</b><br>');

    });
}

function signup() {
    var un = document.getElementById('username').value;

    var em = document.getElementById('email').value;
    var pw = document.getElementById('password').value;
    var pw2 = document.getElementById('password2').value;
    var action = 'reg';
    if (document.getElementById('email').value == '' || document.getElementById('username').value == '' || document.getElementById('password').value == '' || document.getElementById('password2').value == '') {
        document.getElementById('message').innerHTML = '<b>All fields must be compleated</b>';
    } else {
        if (pw == pw2) {
            $.getJSON(domain + '/json_backend.php?callback=?', 'un=' + un + '&pw=' + pw + '&em=' + em + '&action=' + action, function (res) {


                if (res.status == 'Please enter a valid email address') {
                    document.getElementById('message').innerHTML = '<b>' + res.status + '</b>';
                } else {
                    document.getElementById('message').innerHTML = '<b>' + res.status + '</b>';
                    document.getElementById('email').value = '';
                    document.getElementById('username').value = '';
                    document.getElementById('password').value = '';
                    document.getElementById('password2').value = '';
                    document.getElementById('result').innerHTML = '';
                }
                if (res.status == 'Email is already in use') {
                    document.getElementById('message').innerHTML = '<b>' + res.status + '</b>';
                } else {
                    document.getElementById('message').innerHTML = '<b>' + res.status + '</b>';
                    document.getElementById('email').value = '';
                    document.getElementById('username').value = '';
                    document.getElementById('password').value = '';
                    document.getElementById('password2').value = '';
                    document.getElementById('result').innerHTML = '';
                }

            });
        } else {
            document.getElementById('passw').innerHTML = '<b>Passwords do not match</b>';
            document.getElementById('message').innerHTML = '';
            document.getElementById('result').innerHTML = '';
        }
    }


}

function logout()
{
    document.addEventListener("deviceready", logOutReady, false);

}
function logOutReady() {
    window.localStorage.removeItem("username");
    window.localStorage.removeItem("password");
    window.location = "index.html";
}
